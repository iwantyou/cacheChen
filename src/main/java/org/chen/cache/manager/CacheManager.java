package org.chen.cache.manager;

import java.security.InvalidParameterException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * @author: Allen
 * @date: 2017/7/8
 * 缓存管理
 * 此类 应该是有且仅有一个spring bean 对象
 */

public class CacheManager {
    /**
     * 日志对象
     */
    protected static Logger LOG = LoggerFactory.getLogger(CacheManager.class);

    /**
     * Default cache cache.
     */
    private Cache defaultCache;

    /**
     * cache managed by this manager.
     */
    private final ConcurrentMap<String, Cache> caches = new ConcurrentHashMap<String, Cache>();

    // 空值缓存时长
    public static final int nullCacheTimeInSeconds = 60 * 5;

    /**
     * redis client
     */
    private RedisClient redisClient;

    /**
     * 构造方法初始化 缓存管理区
     */
    public CacheManager(RedisConfig redisConfig) {
        init(redisConfig);
    }

    /**
     * 初始化
     */
    private void init(RedisConfig redisConfig) {
        this.redisClient = new RedisClient(redisConfig);
        defaultCache = new CacheChen(CacheChen.DEFAULT_CACHE_NAME);
        caches.put(CacheChen.DEFAULT_CACHE_NAME, defaultCache);

    }


    /**
     * Returns a concrete implementation of Cache, it it is available in the CacheManager.
     *
     * @return a Cache, if an object of that type exists by that name, else null
     */
    public Cache getCache(String name) {
        return this.getCache(name, redisClient.getRedisConfig().getDefaultReadSourceName(), redisClient.getRedisConfig().getDefaultWriteSourceName());
    }

    /**
     * Returns a concrete implementation of Cache, it it is available in the CacheManager.
     *
     * @return a Cache, if an object of that type exists by that name, else null
     */
    public Cache getCache(String name, String readSourceName, String writeSourceName) {
        readSourceName = StringUtils.hasText(readSourceName)? readSourceName : redisClient.getRedisConfig().getDefaultReadSourceName();
        writeSourceName =  StringUtils.hasText(writeSourceName) ? writeSourceName : redisClient.getRedisConfig().getDefaultWriteSourceName();
        Cache cache = caches.get(name);
        if (cache == null) {
            cache = new CacheChen(name, readSourceName, writeSourceName);
            caches.putIfAbsent(name, cache);
            return cache;
        }
        if(!cache.getReadSourceName().equals(readSourceName) || !cache.getWriteSourceName().equals(writeSourceName)){
            throw new InvalidParameterException("配置错误：同一个cacheName:"+name+" 配置的readSourceName 和 writeSourceName 必须一致");
        }
        return cache;
    }


    /**
     * Checks whether a cache of type ehcache exists.
     * <p/>
     *
     * @param cacheName the cache name to check for
     * @return true if it exists
     */
    public boolean cacheExists(String cacheName) {
        return caches.containsKey(cacheName);
    }

    /**
     * Removes all caches using {@link #removeCache(String)} for each cache.
     */
    public void removeAllCaches() {
        String[] cacheNames = getCacheNames();
        for (String cacheName : cacheNames) {
            removeCache(cacheName);
        }
    }


    /**
     * Remove a cache from the CacheManager. The cache is disposed of.
     *
     * @param cacheName the cache name
     */
    public synchronized void removeCache(String cacheName) {

        // NPE guard
        if (cacheName == null || cacheName.length() == 0) {
            return;
        }
        Cache cache = caches.remove(cacheName);
        if (cache != null) {
            cache.clear();
        }
    }

    /**
     * Returns a list of the current cache names.
     *
     * @return an array of {@link String}
     */
    public String[] getCacheNames() {
        return caches.keySet().toArray(new String[0]);
    }


    /**
     * Clears the contents of all caches in the CacheManager with a name starting with the prefix,
     * but without removing them.
     * <p/>
     * This method is not synchronized. It only guarantees to clear those elements in a cache at the time that the
     * {@link CacheChen#clear()} method on each cache is called.
     *
     * @param prefix The prefix the cache name should start with
     * @since 1.7.2
     */
    public void clearAllStartingWith(String prefix) {
        // NPE guard
        if (prefix == null || prefix.length() == 0) {
            return;
        }
        for (Map.Entry<String, Cache> cacheEntry : caches.entrySet()) {
            String cacheName = cacheEntry.getKey();
            if (cacheName.startsWith(prefix)) {
                Cache cache = cacheEntry.getValue();
                cache.clear();
            }

        }
    }


}