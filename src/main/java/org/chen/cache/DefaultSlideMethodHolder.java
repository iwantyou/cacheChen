package org.chen.cache;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.chen.cache.cacheOperation.CacheOperation;
import org.chen.cache.cacheOperation.CacheOperationInvoker;
import org.chen.cache.cacheOperation.CacheableOperation;
import org.chen.cache.manager.Cache;

/**
 * @author: allen.chen
 * @date: 2017/7/9
 * <p>
 * 处理 雪崩
 */
public class DefaultSlideMethodHolder extends AbstractMethodHolder {

    private static final String SLIDE_KEY = "%s_slide";
    /**
     * 雪崩默认的缓存时间
     */
    private static final int DEFAULT_CACHE_TIME = 60;

    private static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 5000,
            60L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());


    /**
     * 批量获取
     *
     * @param keys           方法第一个参数{@link CacheOperationInvoker#args}[0]
     * @param context
     * @return
     * @throws Throwable
     */
    @Override
    public Object batch(Collection<Object> keys, final MethodHolderContext context) throws Throwable {
        Cache cache = context.getCache();
        final CacheableOperation cacheableOperation = getCacheOperation(context.getCacheOperation());
        Map<Object, Object> result = new HashMap<>();
        Collection<Object> syncInvokeKeys = keys.getClass().newInstance();//同步调用key(即key在缓存中不存在)
        final Collection<Object> asynInvokeKeys = keys.getClass().newInstance();//异步调用key
        for (Object key : keys) {
            String slideKey = String.format(SLIDE_KEY, key);
            Object slideValue = cache.get(slideKey);
            Object value = cache.get(key);
            if (slideValue != null) {
                result.put(key, value);
            } else {
                cache.put(slideKey, 1, cacheableOperation.getCacheTime());
                if (cache.containsKey(key)) {
                    asynInvokeKeys.add(key);
                    result.put(key, value);
                } else {
                    syncInvokeKeys.add(key);
                }
            }
        }
        // 异步调用
        if (!asynInvokeKeys.isEmpty()) {
            threadPoolExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    BatchKeyWrapper batchKeyWrapper = new BatchKeyWrapper(asynInvokeKeys);
                    doInvokeAndPutCache(batchKeyWrapper,context);
                }
            });
        }
        // 同步调用
        if (!syncInvokeKeys.isEmpty()) {
            BatchKeyWrapper batchKeyWrapper = new BatchKeyWrapper(syncInvokeKeys);
            InvokeResultWrapper invokeResultWrapper = doInvokeAndPutCache(batchKeyWrapper,context);
            Map<Object, Object> noCacheResult = invokeResultWrapper.getNoCacheResult();
            result.putAll(noCacheResult);
        }

        return this.getResultByReturnType(result, context.getInvoker().getMethod().getReturnType(), keys);
    }

    private CacheableOperation getCacheOperation(CacheOperation cacheOperation) {
        if(cacheOperation.getCacheTime() <= 0){
            /*
                必须有个默认时间,因为 小于等于 0 代表永久存储,雪崩如果是永久存储没有意义
             */
            cacheOperation.setCacheTime(DEFAULT_CACHE_TIME);
        }
        return (CacheableOperation) cacheOperation;
    }

    /**
     * 处理雪崩,将数据存放到缓存中
     *
     * */
    @Override
    protected void doPutCache(InvokeResultWrapper invokeResultWrapper,MethodHolderContext context) {

        int[] randomMilliseconds = {30*1000, 40*1000, 50*1000,60*1000,70*1000,80*1000,90*1000,100*1000};
        ThreadLocalRandom random = ThreadLocalRandom.current();
        for (Map.Entry<Object, Object> noCacheResultEntry : invokeResultWrapper.getNoCacheResult().entrySet()) {
            Object key = noCacheResultEntry.getKey();
            Object value = noCacheResultEntry.getValue();
            int index = random.nextInt(randomMilliseconds.length);
            int randomSecond = randomMilliseconds[index];

            // 缓存null
            if (value == null) {
                if(context.getCacheOperation().isCacheNull()){
                    cacheNull(context.getCache(),key);
                }
            } else {
                context.getCache().put(key, value, context.getCacheOperation().getCacheTime() + randomSecond);
            }
        }

    }

    /**
     * 单个查询
     */
    @Override
    protected Object single(final Object key, final MethodHolderContext context) throws Throwable {
        final CacheableOperation cacheableOperation = getCacheOperation((CacheableOperation) context.getCacheOperation());
        final Cache cache = context.getCache();
        String slideKey = String.format(SLIDE_KEY, key);
        Object slideValue = cache.get(slideKey);
        Object value = cache.get(key);
        if (slideValue != null) {
            return value;
        } else {
            cache.put(slideKey, 1, cacheableOperation.getCacheTime());
            if (cache.containsKey(key)) {
                threadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Object result = context.getInvoker().invokeStoreReturnObject();
                        doPutCache(new InvokeResultWrapper(key,result),context);
                    }
                });
            } else {
                value = context.getInvoker().invokeStoreReturnObject();
                doPutCache(new InvokeResultWrapper(key,value),context);
            }
        }

        return value;
    }

    ;

}
