package org.chen.cache.expression;

import org.chen.cache.cacheOperation.CacheOperationContexts;

/**
 * @author: Allen
 * @date: 2017/7/7
 */
public interface ExpressionEvaluator {

    Object key(CacheOperationContexts.CacheOperationContext cacheOperationContext);

    boolean condition(CacheOperationContexts.CacheOperationContext cacheOperationContext);

    boolean unless(CacheOperationContexts.CacheOperationContext cacheOperationContext);
}
