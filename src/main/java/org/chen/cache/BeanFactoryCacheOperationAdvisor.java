/*
 * Copyright 2002-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.chen.cache;

import org.chen.cache.annotation.CacheEvict;
import org.chen.cache.annotation.CachePut;
import org.chen.cache.annotation.Cacheable;
import org.chen.cache.annotation.Caching;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractBeanFactoryPointcutAdvisor;
import org.springframework.aop.support.StaticMethodMatcherPointcut;

import java.lang.reflect.Method;

/**
 *
 * @author Costin Leau
 * @since 3.1
 */
@SuppressWarnings("serial")
public class BeanFactoryCacheOperationAdvisor extends AbstractBeanFactoryPointcutAdvisor {


	private final StaticMethodMatcherPointcut pointcut = new StaticMethodMatcherPointcut() {

		@Override
		public boolean matches(Method method, Class<?> aClass) {
			if(method.isAnnotationPresent(Cacheable.class)
					|| method.isAnnotationPresent(CacheEvict.class)
					|| method.isAnnotationPresent(CachePut.class)
					|| method.isAnnotationPresent(Caching.class)){
				return true;
			}
			return false;
		}
	};
	/**
	 * Set the {@link ClassFilter} to use for this pointcut.
	 * Default is {@link ClassFilter#TRUE}.
	 */
	public void setClassFilter(ClassFilter classFilter) {
		this.pointcut.setClassFilter(classFilter);
	}
	@Override
	public Pointcut getPointcut() {
		return this.pointcut;
	}

}
