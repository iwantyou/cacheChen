//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.chen.cache;


import org.chen.cache.cacheOperation.CacheOperation;

import java.lang.reflect.Method;
import java.util.Collection;

public interface CacheAnnotationParser {
    Collection<CacheOperation> parseCacheAnnotations(Class<?> clazz);

    Collection<CacheOperation> parseCacheAnnotations(Method method);
}
