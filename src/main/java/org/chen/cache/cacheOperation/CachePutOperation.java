/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.chen.cache.cacheOperation;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Class describing a cache 'get' operation.
 *
 * @author Costin Leau
 * @author Phillip Webb
 * @author Marcin Kamionowski
 * @since 3.1
 * edited by allen at 2017-07-06 基于spring 源码修改
 */
@ToString(callSuper = true)
public class CachePutOperation extends CacheOperation {
	@Setter
	@Getter
	private String unless;
	@Setter
    @Getter
	private String methodHolder;


}
