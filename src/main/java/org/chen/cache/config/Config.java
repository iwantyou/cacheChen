package org.chen.cache.config;

import org.chen.cache.BeanFactoryCacheOperationAdvisor;
import org.chen.cache.CacheInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: allen.chen
 * @date: 2017/7/23
 */
@Configuration
public class Config {

    @Bean
    public BeanFactoryCacheOperationAdvisor cacheAdvisor(){
        BeanFactoryCacheOperationAdvisor advisor = new BeanFactoryCacheOperationAdvisor();
        advisor.setAdvice(cacheInterceptor());
        return advisor;
    }

    @Bean
    public CacheInterceptor cacheInterceptor(){
        return new CacheInterceptor();
    }

}
