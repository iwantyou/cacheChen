package org.chen.cache;

import org.chen.cache.cacheOperation.CacheOperationInvoker;
import org.chen.cache.manager.Cache;
import lombok.SneakyThrows;
import org.apache.commons.beanutils.BeanUtilsBean2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: Allen
 * @date: 2017/7/8
 * <p>
 * 1.调用方法处理缓存
 */
public abstract class AbstractMethodHolder {
    /**
     * 日志对象
     */
    protected Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * method 方法入口
     *
     * @param cacheKey
     * @param context
     * @return
     */
    @SneakyThrows
    public Object invoke(Object cacheKey, MethodHolderContext context) {
        Method method = context.getInvoker().getMethod();
        Object[] args = context.getInvoker().getArgs();

        //批量处理缓存
        if (context.getCacheOperation().isBatch()) {
            if (args.length == 0) {
                throw new InvalidParameterException(method.getName() + " 方法没有参数");
            }
            cacheKey = args[0];
            if (!(cacheKey instanceof Collection)) {
                throw new InvalidParameterException("批量处理缓存:方法的第一个参数类型必须是" + Collection.class.getName());
            }
            Class<?> returnType = method.getReturnType();
            if (Collection.class.isAssignableFrom(returnType) && !StringUtils.hasText(context.getCacheOperation().getResultCacheKey())) {
                throw new InvalidParameterException("批量处理缓存:返回类型是集合时，必须配置resultCacheKey,不能为空");
            }
            if (!Collection.class.isAssignableFrom(returnType) && StringUtils.hasText(context.getCacheOperation().getResultCacheKey())) {
                throw new InvalidParameterException("批量处理缓存:当配置 resultCacheKey，返回类型只能是集合" + Collection.class.getName());
            }
            Collection<Object> keys = (Collection<Object>) cacheKey;
            return this.batch(keys, context);
        }

        return this.single(cacheKey, context);

    }


    /**
     * 单个查询
     */
    protected abstract Object single(Object key, MethodHolderContext context) throws Throwable;

    /**
     * 批量查询，条件：
     * 1、方法第一个参数为Collection
     * 2、返回值类型为Collection 或者 Map
     * 注意：暂时不会缓存结果为null的键值对
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    protected abstract Object batch(Collection<Object> keys, MethodHolderContext context) throws Throwable;

    /**
     * 该方法只能对返回值做简单处理
     */
    @SuppressWarnings("unchecked")
    protected Object getResultByReturnType(Map<Object, Object> result, Class<?> returnType, Collection<Object> keys) throws Exception {
        if (Map.class.isAssignableFrom(returnType)) {
            return result;
        }
        // 返回类型不是接口，可以通过反射直接生成对象
        if (!returnType.isInterface()) {
            return sortPut(result, keys, (Collection<Object>) returnType.newInstance());
        }
        if (Set.class.isAssignableFrom(returnType)) {
            // 返回集合类型的时候，避免返回null值，使用方一般没有做非空判断
            Set<Object> set = new HashSet<Object>(result.size());
            for (Object val : result.values()) {
                if (val != null) {
                    set.add(val);
                }
            }
            return set;
        }
        if (List.class.isAssignableFrom(returnType)) {
            return sortPut(result, keys, new ArrayList<Object>(result.size()));
        }
        // 其他情况暂不处理，抛出异常
        throw new RuntimeException("unsupported result type: " + returnType.getName());
    }

    protected Collection<Object> sortPut(Map<Object, Object> result, Collection<Object> keys, Collection<Object> resultCollection) {
        for (Object key : keys) {
            // 返回集合类型的时候，避免返回null值，使用方一般没有做非空判断
            if (result.containsKey(key) && result.get(key) != null) {
                resultCollection.add(result.get(key));
            }
        }
        return resultCollection;
    }


    /**
     * 缓存空值
     */
    protected void cacheNull(Cache cache, Object... keys) {
        for (Object key : keys) {
            cache.putNull(key);
        }
    }

    /**
     * 将数据放缓存的操作调用此方法
     *
     * @param invokeResultWrapper
     * @param context
     */
    protected void doPutCache(InvokeResultWrapper invokeResultWrapper, MethodHolderContext context) {
        for (Map.Entry<Object, Object> noCacheResultEntry : invokeResultWrapper.getNoCacheResult().entrySet()) {
            Object key = noCacheResultEntry.getKey();
            Object value = noCacheResultEntry.getValue();

            if (value == null) {
                if (context.getCacheOperation().isCacheNull()) {
                    // 缓存null
                    cacheNull(context.getCache(), key);
                }
            } else {
                context.getCache().put(key, value, context.getCacheOperation().getCacheTime());
            }
        }

    }

    protected InvokeResultWrapper doInvokeAndPutCache(BatchKeyWrapper batchKeyWrapper, MethodHolderContext context) {
        //1.调用方法获取对象
        InvokeResultWrapper invokeResultWrapper = this.doInvoke(batchKeyWrapper, context);
        //2.存到缓存中
        this.doPutCache(invokeResultWrapper, context);
        return invokeResultWrapper;
    }

    /**
     * 此方法批量查询的时候使用
     */
    @SneakyThrows
    @SuppressWarnings("unchecked")
    private InvokeResultWrapper doInvoke(BatchKeyWrapper batchKeyWrapper, MethodHolderContext context) {
        CacheOperationInvoker invoker = context.getInvoker();
        InvokeResultWrapper invokeResultWrapper = new InvokeResultWrapper();
        Object invokeResult = null;
        Collection<Object> leftKeys = batchKeyWrapper.getLeftKeys();
        if (leftKeys.size() == batchKeyWrapper.getAllKeys().size()) {
            //缓存全部没有命中
            invokeResult = invoker.invokeStoreReturnObject();
            invokeResultWrapper.setInvokeKeys(leftKeys);
        } else if (!leftKeys.isEmpty()) {
            //部分命中缓存，查询没有命中缓存的数据
            Collection<Object> invokeKeys = leftKeys.getClass().newInstance();
            // 剥离查询的key，因为有可能多个注解相同key同时调用，从invoker.getInvokeCacheResult()中获取
            for (Object leftKey : leftKeys) {
                if (invoker.getInvokeCacheResult().containsKey(leftKey)) {
                    Object result = invoker.getInvokeCacheResult().get(leftKey);
                    invokeResultWrapper.getNoCacheResult().put(leftKey, result);
                } else {
                    invokeKeys.add(leftKey);
                }
            }
            if (!invokeKeys.isEmpty()) {
                Object[] args = invoker.getArgs();
                Object tempFirstArg = args[0];
                args[0] = invokeKeys;
                invokeResult = invoker.invoke();
                args[0] = tempFirstArg;//参数还原
                invokeResultWrapper.setInvokeKeys(invokeKeys);
            }
        }
        invokeResultWrapper.setInvokeResult(invokeResult);

        if (invokeResult == null) {
            for (Object invokeKey : invokeResultWrapper.getInvokeKeys()) {
                invokeResultWrapper.getNoCacheResult().put(invokeKey, null);
                invoker.getInvokeCacheResult().put(invokeKey, null);
            }
            return invokeResultWrapper;
        }
        //处理调用的返回对象*****************开始*****************
        Class<?> returnType = invoker.getMethod().getReturnType();
        if (Map.class.isAssignableFrom(returnType)) {
            // 1、返回结果为Map
            Map<? extends Object, ? extends Object> resultMap = (Map<? extends Object, ? extends Object>) invokeResult;
            invokeResultWrapper.getNoCacheResult().putAll(resultMap);
            invoker.getInvokeCacheResult().putAll(resultMap);
        } else if (Collection.class.isAssignableFrom(returnType)) {
            // 2、返回结果为Collection
            Collection<Object> resultCollection = (Collection<Object>) invokeResult;

            for (Object toCacheObject : resultCollection) {
                if (toCacheObject == null) {
                    continue;
                }
                Object key = BeanUtilsBean2.getInstance().getPropertyUtils().getNestedProperty(toCacheObject, context.getCacheOperation().getResultCacheKey());
                invokeResultWrapper.getNoCacheResult().put(key, toCacheObject);
                invoker.getInvokeCacheResult().put(key, toCacheObject);
            }
        }

        //处理返回为空的对象
        for (Object invokeKey : invokeResultWrapper.getInvokeKeys()) {
            // 不包含key
            if (!invokeResultWrapper.getNoCacheResult().containsKey(invokeKey)) {
                invokeResultWrapper.getNoCacheResult().put(invokeKey, null);
                invoker.getInvokeCacheResult().put(invokeKey, null);
            }
        }

        //处理调用的返回对象*****************结束*****************
        return invokeResultWrapper;
    }


    /**
     * 调用结果封装
     */
    protected static class InvokeResultWrapper {
        private Object invokeResult;

        /**
         * 没有在redis缓存中存在的对象
         */
        private Map<Object, Object> noCacheResult = new HashMap<>();

        /**
         * 默认创建 ArrayList,调用过程中类型可能被覆盖,参考方法 {@link #setInvokeKeys}
         */
        private Collection<Object> invokeKeys = new ArrayList<>();

        public InvokeResultWrapper() {

        }

        public InvokeResultWrapper(Object key, Object invokeResult) {
            invokeKeys.add(key);

            noCacheResult.put(key, invokeResult);

            this.invokeResult = invokeResult;

        }


        public Map<Object, Object> getNoCacheResult() {
            return noCacheResult;
        }


        public Collection<Object> getInvokeKeys() {
            return invokeKeys;
        }

        public void setInvokeKeys(Collection<Object> invokeKeys) {
            this.invokeKeys = invokeKeys;
        }

        public Object getInvokeResult() {
            return invokeResult;
        }

        public void setInvokeResult(Object invokeResult) {
            this.invokeResult = invokeResult;
        }


    }

    /**
     * 批量key
     */
    protected static class BatchKeyWrapper {

        private Collection<Object> allKeys;

        private Collection<Object> leftKeys;


        public BatchKeyWrapper(Collection<Object> allKeys) {
            this(allKeys, allKeys);
        }

        public BatchKeyWrapper(Collection<Object> allKeys, Collection<Object> leftKeys) {
            this.allKeys = allKeys;
            this.leftKeys = leftKeys;
        }

        public Collection<Object> getLeftKeys() {
            return leftKeys;
        }

        public Collection<Object> getAllKeys() {
            return allKeys;
        }


    }


}
