package org.chen.cache;

import org.springframework.util.ObjectUtils;

import java.lang.reflect.Method;

/**
 * 方法描述信息
 */
public class MethodMetadata {
    private final Method method;
    private final Class<?> targetClass;
    private final Object[] args;
    private final Object target;

    public MethodMetadata(Class<?> targetClass, Method method, Object target, Object[] args) {
        this.method = method;
        this.targetClass = targetClass;
        this.target = target;
        this.args = extractArgs(method, args);//克隆参数
    }

    /*
       克隆参数
    */
    private Object[] extractArgs(Method method, Object[] args) {
        if (!method.isVarArgs()) {
            return args;
        }
        Object[] varArgs = ObjectUtils.toObjectArray(args[args.length - 1]);
        Object[] combinedArgs = new Object[args.length - 1 + varArgs.length];
        System.arraycopy(args, 0, combinedArgs, 0, args.length - 1);
        System.arraycopy(varArgs, 0, combinedArgs, args.length - 1, varArgs.length);
        return combinedArgs;
    }

    public Method getMethod() {
        return method;
    }

    public Class<?> getTargetClass() {
        return targetClass;
    }

    public Object[] getArgs() {
        return args;
    }

    public Object getTarget() {
        return target;
    }
}