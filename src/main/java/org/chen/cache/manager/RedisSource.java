package org.chen.cache.manager;

import lombok.Data;
import lombok.Getter;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author: Allen
 * @date: 2017/7/8
 */
@Getter
@Data
public class RedisSource {

    /**
     * 系统默认的读写 名字
     */
    public enum DefaultSourceName{
        DEFAULT_READ,
        DEFAULT_WRITE
    }

    /**
     * 连接的redis 名字，不同的redis库，名字不一样
     */
    private String sourceName;

    /**
     * redis实例IP
     */
    private String ip;
    /**
     * redis端口
     */
    private int port;
    /**
     * redis密码
     */
    private String password;

    /**
     * 超时时间
     */
    private int timeout;


    /**
     * redis 连接池配置
     */
    private JedisPoolConfig poolConfig;



}
