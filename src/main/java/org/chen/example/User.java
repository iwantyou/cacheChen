package org.chen.example;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: allen.chen
 * @date: 2017/7/22
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 7343682728203258913L;
    private Integer id;
    private String name;
    private String email;
    private String phone;
    private Date createTime;
}
