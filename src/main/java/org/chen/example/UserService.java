package org.chen.example;

import org.chen.cache.annotation.CacheEvict;
import org.chen.cache.annotation.CachePut;
import org.chen.cache.annotation.Cacheable;
import org.chen.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @Author: allen.chen
 * @Date: 2017/6/22
 */
@Service
public class UserService {

    //-------- Cacheable demo 开始 -------------------------------------------------------------


    @Cacheable(key = "#userId",cacheTime = 1000 * 50)
    public User get(Integer userId){
        return get(userId, null);
    }

    @Cacheable(key = "#userId",readSourceName = "test1",writeSourceName = "test2")
    public User getBySourceName(Integer userId){
        return get(userId, null);
    }

    @Cacheable(key = "#userId",sync = true)
    public User getSync(Integer userId){
        return get(userId, null);
    }

    @Caching(cacheable={@Cacheable(key = "#userId"),@Cacheable(key = "#email")})
    public User get(Integer userId, String email){
        User user = new User();
        user.setId(userId);
        user.setEmail(email);
        user.setName("测试");
        user.setCreateTime(new Date());
        return user;
    }

    @Cacheable(resultCacheKey = "id",batch = true)
    public ArrayList<User> batchGet(Collection<Integer> userIds){
        ArrayList<User> list = new ArrayList<>();
        for (Integer userId : userIds) {
            User user = new User();
            user.setId(userId);
            user.setName("批量测试");
            user.setCreateTime(new Date());
            list.add(user);
        }

        return list;
    }

    @Cacheable(resultCacheKey = "id",cacheNull = true,batch = true)
    public ArrayList<User> batchGetCacheNull(Collection<Integer> userIds){

        return batchGet(userIds);
    }


    @Cacheable(resultCacheKey = "id",slide = true,batch = true)
    public List<User> batchSlide(Collection<Integer> userIds){
        return  batchGet(userIds);
    }


    //-------- Cacheable demo 结束 -------------------------------------------------------------


    //--------   CachePut demo 开始 -------------------------------------------------------------

    @CachePut(cacheNames = "test",resultCacheKey = "id",batch = true)
    public List<User> batchPut(Collection<Integer> userIds){
        ArrayList<User> list = new ArrayList<>();
        for (Integer userId : userIds) {
            User user = new User();
            user.setId(userId);
            user.setName("批量put测试");
            user.setCreateTime(new Date());
            list.add(user);
        }

        return list;
    }

   @Caching(put={@CachePut(resultCacheKey = "userId"),@CachePut(resultCacheKey = "email")})
    public List<User> batchPutMuiltResultCacheKey(Collection<Integer> userIds){
        return batchPut(userIds);
    }



    //--------   CachePut demo 结束 -------------------------------------------------------------

    //--------   CacheEvict demo 开始 -------------------------------------------------------------

    @CacheEvict(key="#userId")
    public void remove(Integer userId){

    }

    @Caching(evict={@CacheEvict(key="#userId"),@CacheEvict(key="#email")})
    public void batchRemove(Integer userId, String email){

    }

    //--------   CacheEvict demo 结束 -------------------------------------------------------------



}
