/**
 *  Copyright Terracotta, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.chen.cache.concurrent;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Provides a number of Sync which allow fine-grained concurrency. Rather than locking a cache or a store,
 * the individual elements or constituent objects can be locked. This dramatically increases
 * the possible concurrency.
 * <p/>
 * The more stripes, the higher the concurrency. To be threadsafe, the instance of CacheLockProvider needs to be
 * maintained for the entire life of the cache or store, so there is some added memory use.
 * <p/>
 * Though a new class, this code has been refactored from <code>BlockingCache</code>, where it has been in use
 * in highly concurrent production environments for years.
 * <p/>
 * Based on the lock striping concept from Brian Goetz. See Java Concurrency in Practice 11.4.3
 * @author Alex Snaps
 *
 * edited by allen.chen at 2017.07.22
 */
public class StripedReadWriteLockSync implements StripedReadWriteLock {

    private ConcurrentHashMap<Object,ReadWriteLockSync> keyLockSync = new ConcurrentHashMap<>(100);

    /**
     * Gets the Sync Stripe to use for a given key.
     * <p/>
     * This lookup must always return the same Sync for a given key.
     * <p/>
     * @param key the key
     * @return one of a limited number of Sync's.
     */
    public ReadWriteLockSync getSyncForKey(final Object key) {
        ReadWriteLockSync readWriteLockSync = keyLockSync.get(key);
        if (readWriteLockSync == null) {
            readWriteLockSync = new ReadWriteLockSync();
            ReadWriteLockSync oldReadWriteLockSync = keyLockSync.putIfAbsent(key, readWriteLockSync);
            if(oldReadWriteLockSync != null){
                return oldReadWriteLockSync;
            }
        }
        return readWriteLockSync;
    }


    /**
     * Returns all internal syncs
     * @return all internal syncs
     */
    public Collection<ReadWriteLockSync> getAllSyncs() {
        return keyLockSync.values();
    }
}
