package org.chen.cache.manager;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * @author: allen.chen
 * @date: 2017/7/22
 */
@EqualsAndHashCode
@Getter
@ToString
public class CacheKey {
    private String cacheName;
    private Object key;

    public CacheKey(String cacheName, Object key) {
        this.cacheName = cacheName;
        this.key = key;
    }

}
