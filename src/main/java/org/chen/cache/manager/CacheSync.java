package org.chen.cache.manager;

import org.chen.cache.concurrent.LockType;
import org.chen.cache.concurrent.StripedReadWriteLock;
import org.chen.cache.concurrent.StripedReadWriteLockSync;
import org.chen.cache.concurrent.Sync;

/**
 * @author: allen.chen
 * @date: 2017/7/22
 */
public abstract class CacheSync implements CacheSyncInte{
    /**
     * 用来创建 {@link #readWriteLock 的时候使用}
     */
    private final Object lock = new Object();
    /**
     * 缓存锁
     */
    private volatile StripedReadWriteLock readWriteLock;

    /**
     * Gets the lock for a given key
     *
     * @param key
     * @return the lock object for the passed in key
     */
    protected Sync getLockForKey(final Object key) {
        if(readWriteLock == null){
            synchronized (lock){
                //双重检测是否为空
                if(readWriteLock == null){
                    readWriteLock = new StripedReadWriteLockSync();
                }
            }
        }
        return readWriteLock.getSyncForKey(key);
    }

    private void acquireLockOnKey(Object key, LockType lockType) {
        Sync s = getLockForKey(key);
        s.lock(lockType);
    }

    private void releaseLockOnKey(Object key, LockType lockType) {
        Sync s = getLockForKey(key);
        s.unlock(lockType);
    }

    /**
     * Acquires the proper read lock for a given cache key
     *
     * @param key - The key that retrieves a value that you want to protect via locking
     */
    public void acquireReadLockOnKey(Object key) {
        this.acquireLockOnKey(key, LockType.READ);
    }

    /**
     * Acquires the proper write lock for a given cache key
     *
     * @param key - The key that retrieves a value that you want to protect via locking
     */
    public void acquireWriteLockOnKey(Object key) {
        this.acquireLockOnKey(key, LockType.WRITE);
    }

    /**
     * Try to get a read lock on a given key. If can't get it in timeout millis then
     * return a boolean telling that it didn't get the lock
     *
     * @param key - The key that retrieves a value that you want to protect via locking
     * @param timeout - millis until giveup on getting the lock
     * @return whether the lock was awarded
     * @throws InterruptedException
     */
    public boolean tryReadLockOnKey(Object key, long timeout) throws InterruptedException {
        Sync s = getLockForKey(key);
        return s.tryLock(LockType.READ, timeout);
    }

    /**
     * Try to get a write lock on a given key. If can't get it in timeout millis then
     * return a boolean telling that it didn't get the lock
     *
     * @param key - The key that retrieves a value that you want to protect via locking
     * @param timeout - millis until giveup on getting the lock
     * @return whether the lock was awarded
     * @throws InterruptedException
     */
    public boolean tryWriteLockOnKey(Object key, long timeout) throws InterruptedException {
        Sync s = getLockForKey(key);
        return s.tryLock(LockType.WRITE, timeout);
    }

    /**
     * Release a held read lock for the passed in key
     *
     * @param key - The key that retrieves a value that you want to protect via locking
     */
    public void releaseReadLockOnKey(Object key) {
        releaseLockOnKey(key, LockType.READ);
    }

    /**
     * Release a held write lock for the passed in key
     *
     * @param key - The key that retrieves a value that you want to protect via locking
     */
    public void releaseWriteLockOnKey(Object key) {
        releaseLockOnKey(key, LockType.WRITE);
    }


    /**
     * {@inheritDoc}
     * <p>
     * Only Terracotta clustered cache instances currently support querying a thread's read lock hold status.
     */
    public boolean isReadLockedByCurrentThread(Object key) throws UnsupportedOperationException {
        return getLockForKey(key).isHeldByCurrentThread(LockType.READ);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isWriteLockedByCurrentThread(Object key) {
        return getLockForKey(key).isHeldByCurrentThread(LockType.WRITE);
    }
}
