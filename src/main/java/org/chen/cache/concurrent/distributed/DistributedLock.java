package org.chen.cache.concurrent.distributed;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;
import redis.clients.jedis.Jedis;

/**
 *
 * Redis distributed lock implementation.
 *
 * @author zhengcanrui
 * @author: allen.chen
 * @date: 2017/8/11
 *
 */

public class DistributedLock {

    private static Logger logger = LoggerFactory.getLogger(DistributedLock.class);

    private static final int DEFAULT_ACQUIRY_RESOLUTION_MILLIS = 100;

    private static final String LOCK_KEY_TEMPLATE = "%s_lock";
    private static final String LOCK_KEY_COUNT_TEMPLATE = "%s_count";

    /**
     * Lock key path.
     */
    private String lockKey;

    /**
     * 锁超时时间，防止线程在入锁以后，无限的执行等待
     */
    private int expire = 60 * 1000;

    /**
     * 锁等待时间，防止线程饥饿
     */
    private int timeout = 10 * 1000;

    private volatile boolean locked = false;

    /**
     * Detailed constructor with default acquire timeout 10000 msecs and lock expiration of 60000 msecs.
     *
     * @param lockKey lock key (ex. account:1, ...)
     */
    public DistributedLock(String lockKey) {
        this.lockKey = String.format(LOCK_KEY_TEMPLATE,lockKey);
    }

    /**
     * Detailed constructor with default lock expiration of 60000 msecs.
     *
     */
    public DistributedLock(String lockKey, int timeout) {
        this(lockKey);
        this.timeout = timeout;
    }

    /**
     * Detailed constructor.
     *
     */
    public DistributedLock(String lockKey, int timeout, int expire) {
        this(lockKey, timeout);
        this.expire = expire;
    }

    /**
     * @return lock key
     */
    public String getLockKey() {
        return lockKey;
    }

    /**
     * 获得 lock.
     * 实现思路: 主要是使用了redis 的setnx命令,缓存了锁.
     * reids缓存的key是锁的key,所有的共享value是锁的到期时间(注意:这里把过期时间放在value了,没有时间上设置其超时时间)
     * 执行过程:
     * 1.通过setnx尝试设置某个key的值,成功(当前没有这个锁)则返回,成功获得锁
     * 2.锁已经存在则获取锁的到期时间,和当前时间比较,超时的话,则设置新的值
     *
     * @return true if lock is acquired, false acquire timeouted
     * @throws InterruptedException in case of thread interruption
     */
    @SneakyThrows
    public boolean lock(Jedis jedis) {
        while (this.timeout >= 0) {
            long expireEndTime = System.currentTimeMillis() + expire;
            String expireEndTimeStr = String.valueOf(expireEndTime); //锁到期时间
            if (jedis.setnx(lockKey, expireEndTimeStr) > 0) {
                // lock acquired
                locked = true;
                return true;
            }


            // 判断是否过期
            if (checkExpire(jedis)) {
                /*
                 * [分布式的情况下]:同一时间，多个线程恰好都到了这里，但是只有一个线程才能获得锁,所以这里使用 incr 判断
                 */
                String countKey =  String.format(LOCK_KEY_COUNT_TEMPLATE,lockKey);
                int i = jedis.incr(countKey).intValue();
                if(i == 1){
                    /*
                       二次验证，线程A获得锁后，执行set方法，在同一时间第二个线程B进来
                       1.线程A 执行 del(countKey),线程B刚好执行jedis.incr(countKey).intValue(),就会导致 if(i==1) 为true
                       所以这里需判断二次验证是否过期
                     */
                    if(checkExpire(jedis)){
                        // lock acquired
                        locked = true;
                        jedis.set(lockKey,expireEndTimeStr);
                        jedis.del(countKey);
                        return true;
                    }
                    jedis.del(countKey);

                }

            }
            timeout -= DEFAULT_ACQUIRY_RESOLUTION_MILLIS;
             /*
                延迟100 毫秒
             */
            Thread.sleep(DEFAULT_ACQUIRY_RESOLUTION_MILLIS);
        }
        return false;
    }

    private boolean checkExpire(Jedis jedis) {
        String expireEndTimeStrFromRedis = jedis.get(lockKey); //存储在 redis 的过期时间
        return Long.parseLong(expireEndTimeStrFromRedis) < System.currentTimeMillis();
    }


    /**
     * Acqurired lock release.
     */
    public void unlock(Jedis jedis) {
        if (locked) {
            jedis.del(lockKey);
            locked = false;
        }
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof DistributedLock)) {
            return false;
        }
        DistributedLock otherCache = (DistributedLock) other;
        return ObjectUtils.nullSafeEquals(this.lockKey,otherCache.lockKey);
    }

    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = result * PRIME + super.hashCode();
        result = result * PRIME + this.lockKey.hashCode();
        return result;
    }



}