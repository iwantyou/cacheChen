/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.chen.cache.cacheOperation;

import org.chen.cache.AbstractMethodHolder;
import lombok.Getter;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract the invocation of a cache operation.
 *
 * <p>Does not provide a way to transmit checked exceptions but
 * provide a special exception that should be used to wrap any
 * exception that was thrown by the underlying invocation.
 * Callers are expected to handle this issue type specifically.
 *
 * @author Stephane Nicoll
 * @since 4.1
 */
public abstract class CacheOperationInvoker {
	/**
	 * 调用的当前对象
	 */
	@Getter
	private final Object target;
	/**
	 * 调用的方法
	 */
	@Getter
	private final Method method;
	/**
	 * 调用的方法参数
	 */
	@Getter
	private final Object[] args;

	/**
	 * 当批量缓存查询的时候才使用此对象,也就是满足下面条件使用(为什么是满足下面条件才使用请参考{@link AbstractMethodHolder 代码})
	 * 1.{@link CacheOperationInvoker#method}方法第一个参数是Collection
	 * 2.方法的返回类型为Map 和 Collection
	 *
	 * key:方法第一个实际参数(集合参数)的单个元素值
	 * value:返回集合的单个对象(即方法返回的Collection 集合元素对象，如果是 Map 就是Value)
	 * 为什么使用此变量：防止多个注解相同参数 重复调用方法，如果重复调用，就把返回结果存在此变量中
	 */
	@Getter
	private Map<Object, Object> invokeCacheResult = new HashMap<Object, Object>();

	/**
	 * 此对象 是方法的全部参数（没有修改过 {@link CacheOperationInvoker#args}）调用返回的对象
	 * 为什么这里说是全部参数：批量查询的时候 可能是使用未命中缓存的部分参数去查询
	 */
	private Object returnObject;
	/**
	 * 是否已经调用
	 */
	private boolean isInvoke;

	public CacheOperationInvoker(Method method, Object target, Object[] args) {
		this.method = method;
		this.target = target;
		this.args = args;
	}

	/**
	 * Invoke the cache operation defined by this instance. Wraps any exception
	 * that is thrown during the invocation in a {@link ThrowableWrapper}.
	 * @return the result of the operation
	 * @throws ThrowableWrapper if an error occurred while invoking the operation
	 */
	public abstract Object invoke() throws ThrowableWrapper;

	/**
	 *  调用此方法，请确认方法参数{@link CacheOperationInvoker#args}没有被修改
	 *
	 * 	此方法的作用：1.防止多个缓存注解重复调用方法
	 */
	public Object invokeStoreReturnObject(){
		if(returnObject == null && !isInvoke){
			returnObject = invoke();
			isInvoke = true;
		}
		return returnObject;

	}


	/**
	 * Wrap any exception thrown while invoking {@link #invoke()}.
	 */
	@SuppressWarnings("serial")
	public class ThrowableWrapper extends RuntimeException {

		private final Throwable original;

		public ThrowableWrapper(Throwable original) {
			super(original.getMessage(), original);
			this.original = original;
		}

		public Throwable getOriginal() {
			return this.original;
		}
	}

}
