package org.chen.cache.manager.jedis;

import org.chen.cache.manager.RedisClient;
import org.chen.cache.manager.ValueWrapper;
import org.springframework.util.SerializationUtils;
import redis.clients.jedis.Jedis;

/**
 * @author: allen.chen
 * @date: 2017/7/8
 */
public class SimpleCacheNameJedis {

    private String cacheName;

    private String redisSourceName;

    private SimpleCacheNameJedis(String cacheName, String redisSourceName){
        this.cacheName = cacheName;
        this.redisSourceName = redisSourceName;
    }

    public static SimpleCacheNameJedis getInstance(String cacheName){
        return  new SimpleCacheNameJedis(cacheName,null);
    }

    public Long remove(Object key) {
        try (Jedis jedis = getJedis()) {
            Long delNum = jedis.hdel(serialize(cacheName), serialize(key));
            return delNum;
        }
    }

    public Long clearAll() {
        try (Jedis jedis = getJedis()) {
            return jedis.del(serialize(cacheName));
        }

    }

    public <T> T get(Object key) {
        try (Jedis jedis = getJedis()) {
            byte[] cacheNameBytes = serialize(cacheName);
            byte[] keyBytes = serialize(key);
            byte[] valueBytes = jedis.hget(cacheNameBytes, keyBytes);
            if (valueBytes == null) {
                return null;
            }
            ValueWrapper valueWrapper = deserialize(valueBytes);
            Object value = valueWrapper.getValue();
            if(valueWrapper.getEndTime() > 0 && (valueWrapper.getEndTime() - System.currentTimeMillis()) <= 0){
                jedis.hdel(cacheNameBytes, keyBytes);
                value = null;
            }
            return (T) value;
        }
    }

    public void put(Object key, Object value) {
        this.put(key,value,0);
    }

    /**
     *
     * @param key
     * @param value
     * @param cacheTime 毫秒
     * @return
     */
    public Long put(Object key, Object value, int cacheTime) {

        try (Jedis jedis = getJedis()) {
            ValueWrapper valueWrapper = new ValueWrapper(value,cacheTime);
            Long result = jedis.hset(serialize(cacheName), serialize(key), serialize(valueWrapper));
            return result;
        }

    }

    public boolean containsKey(Object key){
        try (Jedis jedis = getJedis()) {
            Boolean exists = jedis.hexists(serialize(cacheName), serialize(key));
            return exists;
        }
    }

    public byte [] serialize(Object obj){
        return  SerializationUtils.serialize(obj);
    }

    public <T> T deserialize(byte [] bytes){
        return (T) SerializationUtils.deserialize(bytes);
    }

    private Jedis getJedis() {
        return redisSourceName == null ? RedisClient.getInstance().getDefaultWriteJedis() : RedisClient.getInstance().getJedis(redisSourceName);
    }

}
