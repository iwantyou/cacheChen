package org.chen.cache.manager;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import org.chen.cache.concurrent.distributed.DistributedLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;
import org.springframework.util.SerializationUtils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

/**
 * @author: allen.chen
 * @date: 2017/7/8
 */
public final class CacheChen extends CacheSync implements Cache,Comparable<CacheChen>,Cloneable {
    /**
     * 日志对象
     */
    protected Logger LOG = LoggerFactory.getLogger(getClass());

    /**
     * A reserved word for cache names. It denotes a default RedisClient.getInstance()
     * which is applied to caches created without RedisClient.getInstance().
     */
    public static final String DEFAULT_CACHE_NAME = "default";

    /**最多容忍50MS延迟*/
    public int WARN_TIME_LIMIT = 50;

    /**
     * 缓存名字
     * 所有的缓存的名字应该唯一
     */
    private String cacheName;

    /**
     * 读的 redis 库名
     */
    private String readSourceName;
    /**
     * 写的 redis 库名
     */
    private String writeSourceName;

    /**
     * 构造方法
     * @param cacheName 缓存名字
     */
    public CacheChen(String cacheName, String readSourceName, String writeSourceName){
        this.cacheName = cacheName;
        this.readSourceName = readSourceName;
        this.writeSourceName = writeSourceName;
    }

    public CacheChen(String cacheName){
        this.cacheName = cacheName;
        this.readSourceName = RedisClient.getInstance().getRedisConfig().getDefaultReadSourceName();
        this.writeSourceName = RedisClient.getInstance().getRedisConfig().getDefaultWriteSourceName();
    }

    /**
     * 是否包含存在key
     * @param key
     * @return
     */
    @Override
    public boolean containsKey(Object key) {
        if (key == null) {
            return false;
        }
        Jedis jedis = RedisClient.getInstance().getJedis(this.readSourceName);
        try {
            Boolean exists = jedis.hexists(getCacheNameBytes(), getCacheKeyBytes(key));
            return exists;
        }finally {
            jedis.close();
        }

    }

    @Override
    public boolean containsKeys(Collection<? extends  Object> keys) {
        if (keys == null || keys.size() == 0) {
            return false;
        }
        Jedis jedis = RedisClient.getInstance().getJedis(this.readSourceName);
        try {
            //redis批处理
            Pipeline pipeline = jedis.pipelined();
            Set<Response<Boolean>> responses = new HashSet<Response<Boolean>>(keys.size());
            for (Object key : keys) {
                responses.add(pipeline.hexists(getCacheNameBytes(), getCacheKeyBytes(key)));
            }
            pipeline.sync();
            for (Response<Boolean> response : responses) {
                if (!response.get()) {
                    return false;
                }
            }
        }finally {
            jedis.close();
        }
        return true;
    }

    @Override
    public boolean containsKeys(Object... keys) {
        HashSet<Object> set = new HashSet<>(keys.length);
        Collections.addAll(set,keys);
        return containsKeys(set);
    }

    /**
     * 从缓存中获取封装的值对象{@link ValueWrapper}
     * @param key
     * @return
     */
    @Override
    public ValueWrapper getValueWrapper(Object key) {
        if (key == null) {
            return null;
        }
        long time = System.currentTimeMillis();
        Jedis jedis = RedisClient.getInstance().getJedis(this.readSourceName);
        try {
            byte[] valueBytes = jedis.hget(getCacheNameBytes(), getCacheKeyBytes(key));
            if (valueBytes == null) {
                return null;
            }
            ValueWrapper valueWrapper = this.getCacheValueObject(valueBytes);
            if(valueWrapper.getEndTime() > 0 && (valueWrapper.getEndTime() - System.currentTimeMillis()) <= 0){
                return null;
            }
            return valueWrapper;
        } finally {
            jedis.close();
            long spend = System.currentTimeMillis() - time;
            if (spend > WARN_TIME_LIMIT) {
                LOG.warn("cacheName [ {} ] get[ {} ] spend[ {}ms ].", new Object[] { cacheName, key, spend });
            }

        }
    }

    /**
     * 从缓存中获取值
     * @param key
     * @return
     */
    @Override
    public Object get(Object key) {
        if (key == null) {
            return null;
        }
        long time = System.currentTimeMillis();
        Jedis jedis = RedisClient.getInstance().getJedis(this.readSourceName);
        try {
            byte[] valueBytes = jedis.hget(getCacheNameBytes(), getCacheKeyBytes(key));
            if (valueBytes == null) {
                return null;
            }
            Object value = this.doExpireValue(jedis,key, valueBytes);
            return value;
        } finally {
            jedis.close();
            long spend = System.currentTimeMillis() - time;
            if (spend > WARN_TIME_LIMIT) {
                LOG.warn("cacheName [ {} ] get[ {} ] spend[ {}ms ].", new Object[] { cacheName, key, spend });
            }

        }
    }

    /**
     * 处理过期的值
     *
     * @param jedis
     * @param key
     * @param valueBytes
     * @return
     */
    private Object doExpireValue(Jedis jedis, Object key, byte[] valueBytes) {
        if(valueBytes == null){
            throw new IllegalArgumentException();
        }
        ValueWrapper valueWrapper = this.getCacheValueObject(valueBytes);
        Object value = valueWrapper.getValue();
        if(valueWrapper.getEndTime() > 0 && (valueWrapper.getEndTime() - System.currentTimeMillis()) <= 0){
            jedis.hdel(getCacheNameBytes(), getCacheKeyBytes(key));
            value = null;
        }
        return value;
    }
    @Override
    public <T> T get(Object key, Callable<T> callable){
        return this.get(key,callable,0);
    }

    /**
     * 调用此方法，将使用缓存同步操作key
     * @param key
     * @param callable
     * @param <T>
     * @return
     */
    @Override
    public <T> T get(Object key, Callable<T> callable, int cacheTime) {
        T result = (T) get(key);
        if (result != null) {
            return result;
        }
        if(RedisClient.getInstance().getRedisConfig().isDistributedLock()){
            return getByDistributedLock(key,callable,cacheTime);
        }
        return getByLocalLock(key, callable,cacheTime);
    }

    /**
     * 本地锁key
     * @param key
     * @param callable
     * @param <T>
     * @return
     */
    private <T> T getByLocalLock(Object key, Callable<T> callable, int cacheTime) {
        T result;
        this.acquireWriteLockOnKey(key);
        try {
            result = (T) get(key); // One more attempt with the write lock
            if (result != null) {
                return result;
            }

            try {
                result = callable.call();
            } catch (Exception e) {
                LOG.error(e.getMessage(),e);
            }

            put(key, result,cacheTime);
            return result;

        }finally {
            this.releaseWriteLockOnKey(key);
        }
    }

    /**
     * 分布式锁key
     * @param key
     * @param callable
     * @param <T>
     * @return
     */
    private <T> T getByDistributedLock(Object key, Callable<T> callable, int cacheTime) {
        T result;
        DistributedLock distributedLock = new DistributedLock(new CacheKey(this.getCacheName(),key).toString());
        Jedis jedis = RedisClient.getInstance().getJedis(this.writeSourceName);
        try {
            boolean lock = distributedLock.lock(jedis);
            if(!lock){
//                String msg = "%s, 获取分布式锁失败 cacheName: %s ,key:%s";
                throw new RuntimeException(String.format(this.toString(),this.getCacheName(),key));
            }
            result = (T) get(key); // One more attempt with the write lock
            if (result != null) {
                return result;
            }

            try {
                result = callable.call();
            } catch (Exception e) {
                LOG.error(e.getMessage(),e);
            }

            put(key, result,cacheTime);
            return result;

        }finally {
            distributedLock.unlock(jedis);
            jedis.close();
        }
    }

    @Override
    public Set<Object> getSet(Collection<? extends  Object> keys) {
        if (keys == null || keys.size() == 0) {
            return new HashSet<Object>(0);
        }
        Set<Object> result = new LinkedHashSet<Object>(keys.size());
        Set<byte[]> keySet = new HashSet<byte[]>(keys.size());
        for (Object key : keys) {
            keySet.add(this.getCacheKeyBytes(key));
        }
        if(result.size() == keys.size()){
            return result;
        }

        Jedis jedis = RedisClient.getInstance().getJedis(this.readSourceName);
        try {
            //redis批处理
            Pipeline pipeline = jedis.pipelined();
            Map<Object,Response<byte[]>> responses = new HashMap<>();
            for (Object key : keySet) {
                Response<byte[]> response = pipeline.hget(getCacheNameBytes(), getCacheKeyBytes(key));
                responses.put(key,response);
            }
            pipeline.sync();
            for (Map.Entry<Object, Response<byte[]>> responseEntry : responses.entrySet()) {
                Object key = responseEntry.getKey();
                byte[] bytes = responseEntry.getValue().get();
                if(bytes == null){
                    continue;
                }
                Object value = this.doExpireValue(jedis,key,bytes);
                if(value != null){
                    result.add(value);
                }
            }

        }finally {
            jedis.close();
        }
        return result;
    }

    @Override
    public Set<Object> getSet(Object... keys) {
        HashSet<Object> set = new HashSet<>(keys.length);
        Collections.addAll(set,keys);
        return getSet(set);
    }

    @Override
    public HashMap<Object, Object> getMap(Collection<? extends  Object> keys) {
        HashMap<Object, Object> result = new HashMap<Object, Object>(keys.size());

        //redis批处理
        Jedis jedis = RedisClient.getInstance().getJedis(this.readSourceName);
        try {
            Pipeline pipeline = jedis.pipelined();
            Map<Object, Response<byte[]>> responseMap = new HashMap<Object, Response<byte[]>>(keys.size());
            for (Object key : keys) {
                Response<byte[]> resultBytes = pipeline.hget(getCacheNameBytes(), getCacheKeyBytes(key));
                responseMap.put(key, resultBytes);
            }
            pipeline.sync();
            for (Map.Entry<Object, Response<byte[]>> entry : responseMap.entrySet()) {
                Object key = entry.getKey();
                byte[] value = entry.getValue().get();
                if (value == null) {
                    // redis中不存在该Key
                    continue;
                }
                Object val = this.doExpireValue(jedis,key,value);
                if(val != null){
                    result.put(entry.getKey(), val);
                }

            }
        }finally {
            jedis.close();
        }
        return result;
    }

    @Override
    public HashMap<Object, Object> getMap(Object... keys) {
        HashSet<Object> set = new HashSet<>(keys.length);
        Collections.addAll(set,keys);
        return getMap(set);
    }

    @Override
    public void put(Object key, Object value){
        this.put(key,value,0);
    }

    @Override
    public void putNull(Object key){
        // 对于空值，只用本地缓存
        Jedis jedis = RedisClient.getInstance().getJedis(this.writeSourceName);
        try {
            ValueWrapper valueWrapper = new ValueWrapper(null,CacheManager.nullCacheTimeInSeconds);
            Long result = jedis.hset(getCacheNameBytes(), getCacheKeyBytes(key), this.getCacheValueBytes(valueWrapper));
        }finally {
            jedis.close();
        }
    }

    @Override
    public void put(Object key, Object value, int cacheTime) {
        if (key == null) {
            return;
        }
        long time = System.currentTimeMillis();
        Jedis jedis = RedisClient.getInstance().getJedis(this.writeSourceName);
        try {
            ValueWrapper valueWrapper = new ValueWrapper(value,cacheTime);
            Long result = jedis.hset(getCacheNameBytes(), getCacheKeyBytes(key), this.getCacheValueBytes(valueWrapper));
        }finally {
            jedis.close();
        }
        long spend = System.currentTimeMillis() - time;
        if (spend > WARN_TIME_LIMIT) {
            LOG.warn("cacheName[ {} ] get[ {} - {} ] spend[ {}ms ].", new Object[] { getCacheName(), key, value, spend });
        }
    }

    @Override
    public boolean putAll(Map<? extends Object, ? extends Object> map){
        return putAll(map,0);
    }

    @Override
    public boolean putAll(Map<? extends Object, ? extends Object> map, int cacheTime) {
        long time = System.currentTimeMillis();
        if (map == null || map.size() == 0) {
            return false;
        }
        //redis批处理
        Jedis jedis = RedisClient.getInstance().getJedis(this.readSourceName);
        try {
            List<Response<Long>> responses = new ArrayList<>(map.size());
            Pipeline pipeline = jedis.pipelined();
            for (Map.Entry<? extends Object, ? extends Object> entry : map.entrySet()) {
                Object key = entry.getKey();
                Object value = entry.getValue();

                ValueWrapper valueWrapper = new ValueWrapper(value,cacheTime);
                Response<Long> result = pipeline.hset(getCacheNameBytes(), getCacheKeyBytes(key), this.getCacheValueBytes(valueWrapper));
                responses.add(result);
            }
            pipeline.sync();
            return true;
        } finally {
            jedis.close();
            long spend = System.currentTimeMillis() - time;
            if (spend > WARN_TIME_LIMIT) {
                LOG.warn("cacheName[ {} ] putAll, spend[ {}ms ].", new Object[] { this.cacheName, spend });
            }
        }
    }
    @Override
    public void remove(Object key) {
        if (key == null) {
            return;
        }
        Jedis jedis = RedisClient.getInstance().getJedis(this.writeSourceName);
        try {
            Long delNum = jedis.hdel(this.getCacheNameBytes(), this.getCacheKeyBytes(key));
        }finally {
            jedis.close();
        }
    }

    @Override
    public long removes(Collection<? extends  Object> keys) {
        if (keys == null || keys.size() == 0) {
            return 0L;
        }
        long time = System.currentTimeMillis();
        byte[][] keyBytes = new byte[keys.size()][];
        int index = 0;
        for (Object key : keys) {
            keyBytes[index] =  this.getCacheKeyBytes(key);
            index++;
        }
        Jedis jedis = RedisClient.getInstance().getJedis(this.writeSourceName);
        try {
            Long delNum = jedis.hdel(this.getCacheNameBytes(), keyBytes);
            long spend = System.currentTimeMillis() - time;
            if (spend > WARN_TIME_LIMIT) {
                LOG.warn("cacheName [ {} ] remove keys[ {} ], spend[ {}ms ].", new Object[]{cacheName, keys.toString(), spend});
            }
            return delNum;
        }finally {
            jedis.close();
        }
    }


    @Override
    public long removes(Object... keys) {
        HashSet<Object> set = new HashSet<>(keys.length);
        Collections.addAll(set,keys);
        return this.removes(set);
    }

    @Override
    public int size() {
        Jedis jedis = RedisClient.getInstance().getJedis(this.readSourceName);
        try {
            return jedis.hlen(this.getCacheNameBytes()).intValue();
        }finally {
            jedis.close();
        }
    }


    @Override
    public void clear() {

        Jedis jedis = RedisClient.getInstance().getJedis(this.writeSourceName);
        try {
            jedis.del(this.getCacheNameBytes());
        }finally {
            jedis.close();
        }
    }


    @Override
    public Set<Object> keySet() {
        Jedis jedis = RedisClient.getInstance().getJedis(this.readSourceName);
        try {
            Set<byte[]> keys = jedis.hkeys(this.getCacheNameBytes());
            Set<Object> result = new HashSet<>(keys.size());
            for (byte[] keyByte : keys) {
                result.add(this.getCacheKeyObject(keyByte));
            }
            return result;
        }finally {
            jedis.close();
        }
    }

    @Override
    public String getCacheName() {
        return cacheName;
    }

    @Override
    public String getReadSourceName() {
        return readSourceName;
    }

    @Override
    public String getWriteSourceName() {
        return writeSourceName;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof CacheChen)) {
            return false;
        }
        CacheChen otherCache = (CacheChen) other;
        return ObjectUtils.nullSafeEquals(this.cacheName,otherCache.cacheName);
    }

    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = result * PRIME + this.cacheName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "缓存名称："+this.cacheName;
    }

    @Override
    public int compareTo(CacheChen other) {
        int result = this.cacheName.compareTo(other.cacheName);
        return result;
    }

    private byte [] getCacheNameBytes(){
        return SerializationUtils.serialize(cacheName);
    }

    private Object getCacheKeyObject(byte [] keys){
        return  SerializationUtils.deserialize(keys);
    }

    private byte [] getCacheKeyBytes(Object key){
        return  SerializationUtils.serialize(key);
    }

    private byte [] getCacheValueBytes(Object value){
        return SerializationUtils.serialize(value);
    }

    private ValueWrapper getCacheValueObject(byte [] keys){
        return (ValueWrapper) SerializationUtils.deserialize(keys);
    }


}
