package org.chen.cache.cacheOperation;

import org.springframework.context.expression.AnnotatedElementKey;

import java.lang.reflect.Method;

public class CacheOperationCacheKey {

    private final CacheOperation cacheOperation;

    private final AnnotatedElementKey methodCacheKey;

    public CacheOperationCacheKey(CacheOperation cacheOperation, Method method, Class<?> targetClass) {
        this.cacheOperation = cacheOperation;
        this.methodCacheKey = new AnnotatedElementKey(method, targetClass);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof CacheOperationCacheKey)) {
            return false;
        }
        CacheOperationCacheKey otherKey = (CacheOperationCacheKey) other;
        return (this.cacheOperation.equals(otherKey.cacheOperation) &&
                this.methodCacheKey.equals(otherKey.methodCacheKey));
    }

    @Override
    public int hashCode() {
        return (this.cacheOperation.hashCode() * 31 + this.methodCacheKey.hashCode());
    }

    @Override
    public String toString() {
        return this.cacheOperation + " on " + this.methodCacheKey;
    }


}