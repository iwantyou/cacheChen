/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.chen.cache.annotation;

import org.chen.cache.AbstractMethodHolder;
import org.chen.cache.manager.Cache;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation indicating that a method (or all methods on a class) triggers a
 * {@link Cache#put(Object, Object) cache get} operation.
 *
 * <p>In contrast to the {@link Cacheable @Cacheable} annotation, this annotation
 * does not cause the advised method to be skipped. Rather, it always causes the
 * method to be invoked and its result to be stored in the associated cache. Note
 * that Java8's {@code Optional} return types are automatically handled and its
 * content is stored in the cache if present.
 *
 * <p>This annotation may be used as a <em>meta-annotation</em> to create custom
 * <em>composed annotations</em> with attribute overrides.
 *
 * @author Costin Leau
 * @author Phillip Webb
 * @author Stephane Nicoll
 * @author Sam Brannen
 * @author allen.chen
 * @since 3.1
 * @see CacheConfig
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CachePut {

	/**
	 * Alias for {@link #cacheNames}.
	 */
	@AliasFor("cacheNames")
	String[] value() default {};

	/**
	 * 注：请务必确保同一个cacheName ，配置的{@link #writeSourceName} 必须一致
	 * Names of the caches to use for the cache get operation.
	 * <p>Names may be used to determine the target cache (or caches), matching
	 * the qualifier value or bean name of a specific bean definition.
	 * @since 4.2
	 * @see #value
	 * @see CacheConfig#cacheNames
	 */
	@AliasFor("value")
	String[] cacheNames() default {};

	/**
	 * 写的 redis 库名
	 */
	String writeSourceName() default "";

	/**
	 * Spring Expression Language (SpEL) expression for computing the key dynamically.
	 * <p>Default is {@code ""}, meaning  method first parameter(第一个参数) are considered as a key,
	 * unless a custom {@link #keyGenerator} has been configured.
	 * <p>The SpEL expression evaluates against a dedicated context that provides the
	 * following meta-data:
	 * <ul>
	 * <li>{@code #root.method}, {@code #root.target}, and {@code #root.caches} for
	 * references to the {@link java.lang.reflect.Method method}, target object, and
	 * affected cache(s) respectively.</li>
	 * <li>Shortcuts for the method name ({@code #root.methodName}) and target class
	 * ({@code #root.targetClass}) are also available.</li>
	 * <li>Method arguments can be accessed by index. For instance the second argument
	 * can be accessed via {@code #root.args[1]}, {@code #p1} or {@code #a1}. Arguments
	 * can also be accessed by name if that information is available.</li>
	 * <li>当{@link #batch()} 为false，不能为空</li>
	 * </ul>
	 */
	String key() default "";

	/**
	 * The bean name of the custom {@link org.springframework.cache.interceptor.KeyGenerator}
	 * to use.
	 * <p>Mutually exclusive with the {@link #key} attribute.
	 * @see CacheConfig#keyGenerator
	 */
	String keyGenerator() default "";

	/**
	 * The bean name of the custom {@link AbstractMethodHolder} to use to
	 * 必须继承 {@link AbstractMethodHolder}
	 */
	String methodHolder() default "";

	/**
	 * Spring Expression Language (SpEL) expression used for making the cache
	 * get operation conditional.
	 * <p>Default is {@code ""}, meaning the method result is always cached.
	 * <p>The SpEL expression evaluates against a dedicated context that provides the
	 * following meta-data:
	 * <ul>
	 * <li>{@code #root.method}, {@code #root.target}, and {@code #root.caches} for
	 * references to the {@link java.lang.reflect.Method method}, target object, and
	 * affected cache(s) respectively.</li>
	 * <li>Shortcuts for the method name ({@code #root.methodName}) and target class
	 * ({@code #root.targetClass}) are also available.
	 * <li>Method arguments can be accessed by index. For instance the second argument
	 * can be accessed via {@code #root.args[1]}, {@code #p1} or {@code #a1}. Arguments
	 * can also be accessed by name if that information is available.</li>
	 * </ul>
	 */
	String condition() default "";

	/**
	 * Spring Expression Language (SpEL) expression used to veto the cache get operation.
	 * <p>Unlike {@link #condition}, this expression is evaluated after the method
	 * has been called and can therefore refer to the {@code result}.
	 * <p>Default is {@code ""}, meaning that caching is never vetoed.
	 * <p>The SpEL expression evaluates against a dedicated context that provides the
	 * following meta-data:
	 * <ul>
	 * <li>{@code #result} for a reference to the result of the method invocation. For
	 * supported wrappers such as {@code Optional}, {@code #result} refers to the actual
	 * object, not the wrapper</li>
	 * <li>{@code #root.method}, {@code #root.target}, and {@code #root.caches} for
	 * references to the {@link java.lang.reflect.Method method}, target object, and
	 * affected cache(s) respectively.</li>
	 * <li>Shortcuts for the method name ({@code #root.methodName}) and target class
	 * ({@code #root.targetClass}) are also available.
	 * <li>Method arguments can be accessed by index. For instance the second argument
	 * can be accessed via {@code #root.args[1]}, {@code #p1} or {@code #a1}. Arguments
	 * can also be accessed by name if that information is available.</li>
	 * </ul>
	 * @since 3.2
	 */
	String unless() default "";

	/**
	 * 是否缓存null值
	 * @return
	 */
	boolean cacheNull() default false;

	/**
	 *  缓存时间(单位[毫秒])
	 * 默认0,代表永久存储
	 * @return
	 */
	int cacheTime() default 0;

	/**
	 *
	 * 满足下面条件配置此属性
	 * <dl>
	 * <dd>1.必须配置 {@link #batch} 为true</dd>
	 * <dd>2.没有且不能配置 {@link #key} </dd>
	 * <dd>3.方法的第一个参数类型必须是{@link  java.util.Collection},集合元素值将作为缓存的key存储到redis中)</dd>
	 * <dd>4.方法返回类型必须为{@link  java.util.Collection},{@link #resultCacheKey}配置集合中bean对象的属性名字</dd>
	 * <dd>>>4.1 如果是接口</dd>
	 * <dd>>>>>4.1.1 {@link java.util.List}，返回结果是 {@link java.util.ArrayList}</dd>
	 * <dd>>>>>4.1.2 {@link java.util.Set}，返回结果是 {@link java.util.HashSet}</dd>
	 * <dd>>>>>4.1.3 除了上面2种,不支持其他集合接口</dd>
	 * <dd>>>4.2 如果是实现类，例如{@link java.util.LinkedList},那么返回结果也是{@link java.util.LinkedList},建议返回类型使用实现类</dd>
	 * <dd style="font:bold">---------------------在{@link CachePut}中使用注意事项如下--------------------------</dd>
	 * <dd style="color:green">可以配置1个或多个 {@link CachePut} ，可以在组合注解{@link Caching} 中使用</dd>
	 * </dl>
	 * <p>作用: 批量处理缓存</p>
	 *
	 * @return
	 * @see AbstractMethodHolder#batch
	 */
	String resultCacheKey() default "";

	/**
	 * <dl>
	 * <dd>1.没有且不能配置{@link #key}</dd>
	 * <dd>2.方法的第一个参数必须是 {@link  java.util.Collection}，集合元素值将作为缓存的key存储到redis中</dd>
	 * <dd>3.方法的返回类型必须是下面的类型中的一种</dd>
	 * <dd>>>3.1 {@link  java.util.Collection},必须配置{@link #resultCacheKey}, 参考{@link #resultCacheKey}的注释说明</dd>
	 * <dd>>>3.2 {@link  java.util.Map}</dd>
	 * <dd>>>>>3.2.1 如果是接口,返回结果是 {@link java.util.HashMap}</dd>
	 * <dd>>>>>3.2.1 如果是实现类,例如{@link java.util.LinkedHashMap},那么返回结果也是{@link java.util.LinkedHashMap},建议返回类型使用实现类</dd>
	 * <dd>>>>>3.2.1 返回类型为Map时,返回的Map中key 请务必保证和传入的参数(即上面第2点中的集合元素值)必须一致
	 *           map中key将作为缓存key存储到redis中</dd>
	 *  <dd style="font:bold">-----------------在{@link CachePut}中使用注意事项如下------------------------------</dd>
	 * <dd style="color:green">支持组合注解{@link Caching},可以配置一个或多个</dd>
	 * </dl>
	 *
	 * <p>如果有配置{@link #resultCacheKey},处理逻辑请看{@link #resultCacheKey}中的注释</p>
	 * @return
	 */
	boolean batch() default false;



}
