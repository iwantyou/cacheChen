/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.chen.cache;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.concurrent.Callable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chen.cache.cacheOperation.CacheEvictOperation;
import org.chen.cache.cacheOperation.CacheOperationContexts;
import org.chen.cache.cacheOperation.CacheOperationInvoker;
import org.chen.cache.cacheOperation.CachePutOperation;
import org.chen.cache.cacheOperation.CacheableOperation;
import org.chen.cache.expression.CacheOperationExpressionEvaluator;
import org.chen.cache.manager.Cache;
import org.chen.cache.manager.CacheManager;
import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * Base class for caching aspects, such as the {@link CacheInterceptor}
 * or an AspectJ aspect.
 * <p>
 *
 * @author Costin Leau
 * @author Juergen Hoeller
 * @author Chris Beams
 * @author Phillip Webb
 * @author Sam Brannen
 * @author Stephane Nicoll
 * @since 3.1
 * <p>
 * edited by allen at 2017-07-06 基于spring 源码修改
 */
public abstract class AbstractCacheSupport
        implements BeanFactoryAware, InitializingBean, SmartInitializingSingleton, DisposableBean {

    protected final Log logger = LogFactory.getLog(getClass());

    /**
     * 参数表达式计算器
     */
    private final CacheOperationExpressionEvaluator evaluator;


    private KeyGenerator keyGenerator = new SimpleKeyGenerator();

    private CacheManager cacheManager;
    /**
     * 作用：用来解析方法上的缓存
     */
    private DefaultCacheAnnotationParser springCacheAnnotationParser = new DefaultCacheAnnotationParser();
    private DefaultCacheableMethodHolder defaultCacheableMethodHolder = new DefaultCacheableMethodHolder();
    private DefaultCachePutMethodHolder defaultCachePutMethodHolder = new DefaultCachePutMethodHolder();
    private DefaultSlideMethodHolder defaultSlideMethodHolder = new DefaultSlideMethodHolder();

    private BeanFactory beanFactory;
    private boolean initialized = false;

    protected AbstractCacheSupport() {
        evaluator = new CacheOperationExpressionEvaluator(beanFactory);
    }


    /**
     *
     */
    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }


    /**
     * Set the containing {@link BeanFactory} for {@link CacheManager} and other
     * service lookups.
     *
     * @since 4.3
     */
    @Override
    public void setBeanFactory(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    /**
     * 属性设置完后 验证（spring容器调用）
     */
    @Override
    public void afterPropertiesSet() {
        Assert.state(springCacheAnnotationParser != null, "The 'springCacheAnnotationParser' property 不能为空: ");
    }

    /**
     * 对象实例化后调用此方法（spring容器调用）
     */
    @Override
    public void afterSingletonsInstantiated() {
        if (cacheManager == null) {
            // Lazily initialize cache resolver via default cache manager...
            try {
                setCacheManager(this.beanFactory.getBean(CacheManager.class));
            } catch (NoUniqueBeanDefinitionException ex) {
                throw new IllegalStateException("No CacheResolver specified, and no unique bean of type " +
                        "CacheManager found. Mark one as primary (or give it the name 'cacheManager') or " +
                        "declare a specific CacheManager to use, that serves as the default one.");
            } catch (NoSuchBeanDefinitionException ex) {
                throw new IllegalStateException("No CacheResolver specified, and no bean of type CacheManager found. " +
                        "Register a CacheManager bean or remove the @EnableCaching annotation from your configuration.");
            }
        }
        this.initialized = true;
    }


    /**
     * Clear the cached.
     */
    protected void clearCache() {
        this.evaluator.clear();
    }

    /**
     * 缓存拦截方法执行入口
     *
     * @param invoker
     * @return
     */
    protected Object execute(CacheOperationInvoker invoker) {
        // Check whether aspect is enabled (to cope with cases where the AJ is pulled in automatically)
        if (this.initialized) {
            Method method = invoker.getMethod();
            Object target = invoker.getTarget();
            Object[] args = invoker.getArgs();
            Class<?> targetClass = getTargetClass(target);
            CacheOperationContexts.ContextCall contextCall = createContextCall();
            MethodMetadata methodMetadata = new MethodMetadata(targetClass, method, target, args);
            CacheOperationContexts contexts = new CacheOperationContexts(contextCall, methodMetadata);
            return this.execute(invoker, contexts);
        }
        return invoker.invoke();
    }

    private CacheOperationContexts.ContextCall createContextCall() {
        return new CacheOperationContexts.ContextCall() {

            @Override
            public CacheManager getCacheManager() {
                return cacheManager;
            }

            @Override
            public BeanFactory getBeanFactory() {
                return beanFactory;
            }

            @Override
            public KeyGenerator getKeyGenerator() {
                return keyGenerator;
            }

            @Override
            public AbstractMethodHolder getDefaultCacheableMethodHolder() {
                return defaultCacheableMethodHolder;
            }

            @Override
            public AbstractMethodHolder getDefaultCachePutMethodHolder() {
                return defaultCachePutMethodHolder;
            }

            @Override
            public DefaultSlideMethodHolder getDefaultSlideMethodHolder() {
                return defaultSlideMethodHolder;
            }

            @Override
            public DefaultCacheAnnotationParser getCacheAnnotationParser() {
                return springCacheAnnotationParser;
            }
        };
    }


    private Class<?> getTargetClass(Object target) {
        Class<?> targetClass = AopProxyUtils.ultimateTargetClass(target);
        if (targetClass == null && target != null) {
            targetClass = target.getClass();
        }
        return targetClass;
    }

    /**
     * 根据缓存注解 执行缓存操作
     *
     * @param invoker
     * @param contexts
     * @return
     */
    private Object execute(final CacheOperationInvoker invoker, CacheOperationContexts contexts) {
        // Special handling of synchronized invocation
        if (contexts.isSync()) {
            CacheOperationContexts.CacheOperationContext context = contexts.get(CacheableOperation.class).iterator().next();
            if (this.conditionPassing(context)) {
                Object key = getKey(context);
                // 同步缓存，只能配置一个 Cache ，所以这里取第一个即可
                Cache cache = context.getCacheOperationMetadata().getCaches().values().iterator().next();
                Object result = cache.get(key, new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return invoker.invoke();
                    }
                },context.getCacheOperationMetadata().getOperation().getCacheTime());
                return result;

            }
            // No caching required, only call the underlying method
            return invoker.invoke();
        }

        // Process any early evictions
        this.processCacheEvicts(contexts.get(CacheEvictOperation.class), true);

        Object cachePutReturnObject = this.processCachePuts(contexts.get(CachePutOperation.class), invoker);

        Object cacheableReturnObject = this.processCacheables(contexts.get(CacheableOperation.class), invoker);

        // Process any late evictions
        processCacheEvicts(contexts.get(CacheEvictOperation.class), false);
        Object returnObject = this.getReturnObject(cachePutReturnObject, cacheableReturnObject);
        return returnObject == null ? invoker.invokeStoreReturnObject() : returnObject;
    }

    private Object getReturnObject(Object... objects) {
        for (int i = 0; i < objects.length; i++) {
            Object object = objects[i];
            if (object != null) {
                return object;
            }
        }
        return null;
    }

    private Object getKey(CacheOperationContexts.CacheOperationContext context) {
        if (!StringUtils.hasText(context.getCacheOperationMetadata().getOperation().getKey())) {
            return null;
        }
        return evaluator.key(context);
    }

    private Object processCacheables(Collection<CacheOperationContexts.CacheOperationContext> contexts, CacheOperationInvoker invoker) {
        Object returnObject = null;
        for (CacheOperationContexts.CacheOperationContext context : contexts) {
            if (!this.conditionPassing(context)) {
                continue;
            }
            Object key = getKey(context);
            for (Cache cache : context.getCacheOperationMetadata().getCaches().values()) {
                AbstractMethodHolder methodHolder = context.getCacheOperationMetadata().getMethodHolder();
                Object obj = methodHolder.invoke(key,new MethodHolderContext(cache,context.getCacheOperationMetadata().getOperation(),invoker));
                if (obj != null) {
                    returnObject = obj;
                }
            }
        }
        return returnObject;
    }

    /**
     * 是否能放到缓存中
     *
     * @param context
     * @return
     */
    protected boolean conditionPassing(CacheOperationContexts.CacheOperationContext context) {
        return !evaluator.unless(context) && isConditionPassing(context);
    }

    /**
     * 处理 放缓存
     *
     * @param contexts
     * @param invoker
     * @return
     */
    private Object processCachePuts(Collection<CacheOperationContexts.CacheOperationContext> contexts, CacheOperationInvoker invoker) {
        Object returnObject = null;
        for (CacheOperationContexts.CacheOperationContext context : contexts) {
            if (!this.conditionPassing(context)) {
                continue;
            }
            Object key = getKey(context);
            Collection<Cache> caches = context.getCacheOperationMetadata().getCaches().values();
            for (Cache cache : caches) {
                AbstractMethodHolder methodHolder = context.getCacheOperationMetadata().getMethodHolder();
                Object obj = methodHolder.invoke(key, new MethodHolderContext(cache,context.getCacheOperationMetadata().getOperation(),invoker));
                if (obj != null) {
                    returnObject = obj;
                }
            }
        }
        return returnObject;
    }


    private void processCacheEvicts(Collection<CacheOperationContexts.CacheOperationContext> contexts, boolean beforeInvocation) {
        for (CacheOperationContexts.CacheOperationContext context : contexts) {
            CacheEvictOperation operation = (CacheEvictOperation) context.getCacheOperationMetadata().getOperation();
            if (beforeInvocation != operation.isBeforeInvocation() || !isConditionPassing(context)) {
                continue;
            }
            Object key = null;
            Collection<Cache> caches = context.getCacheOperationMetadata().getCaches().values();
            for (Cache cache : caches) {
                if (operation.isCacheWide()) {
                    cache.clear();
                } else {
                    if (key == null) {
                        key = this.getKey(context);
                    }
                    cache.remove(key);
                }
            }
        }
    }


    /**
     * 缓存注解配置了condition 属性，判断是否满足缓存条件
     *
     * @param context
     * @return
     */
    private boolean isConditionPassing(CacheOperationContexts.CacheOperationContext context) {
        boolean passing = evaluator.condition(context);
        if (!passing && logger.isTraceEnabled()) {
            logger.trace("Cache condition failed on method " + context.getMethodMetadata().getMethod() +
                    " for operation " + context.getCacheOperationMetadata().getOperation());
        }
        return passing;
    }


}
