
package org.chen.cache.manager;


import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

public interface Cache extends CacheSyncInte{



    /**
     * Associate the specified value with the specified key in this cache.
     * <p>If the cache previously contained a mapping for this key, the old
     * value is replaced by the specified value.
     * @param key the key with which the specified value is to be associated
     * @param value the value to be associated with the specified key
     */
    void put(Object key, Object value);
    void putNull(Object key);

    void put(Object key, Object value, int cacheTime);

    /**
     * 批量存储
     * @param map
     */
    boolean putAll(Map<? extends Object, ? extends Object> map);
    boolean putAll(Map<? extends Object, ? extends Object> map, int cacheTime);

    /**
     * 是否包含某个指定的key值
     * @param key
     * @return
     */
    boolean containsKey(Object key);

    boolean containsKeys(Collection<? extends Object> keys);

    boolean containsKeys(Object... key);

    /**
     * 从缓存中获取封装的值对象{@link ValueWrapper}
     * @param key
     * @return
     */
    ValueWrapper getValueWrapper(Object key);

    /**
     * 通过缓存key ，获取值
     * @param key
     * @return
     */
    Object get(Object key);


    /**
     * 调用此方法，将使用缓存同步操作key
     * @param key
     * @param valueLoader
     * @param <T>
     * @return
     */
    <T> T get(Object key, Callable<T> valueLoader);

    /**
     * 调用此方法，将使用缓存同步操作key
     * @param key
     * @param valueLoader
     * @param <T>
     * @return
     */
    <T> T get(Object key, Callable<T> valueLoader, int cacheTime);

    /**
     * 批量获取缓存中的值，走本地缓存
     * @param
     * @param keys
     * @return	返回结果无序
     */
    Set<Object> getSet(Collection<? extends Object> keys);

    /**
     * 批量获取缓存，走本地缓存
     * @param keys
     * @return	返回结果无序
     */
    Set<Object> getSet(Object... keys);


    /**
     * 批量获取，走本地缓存
     * @param keys
     * @return	缓存中存在的K-V键值对，size <= keys.size()
     */
    HashMap<Object, Object> getMap(Collection<? extends Object> keys);

    HashMap<Object, Object> getMap(Object... keys);


    void remove(Object key);
    /**
     * 从缓存中批量移除指定key值
     * @param keys
     */
    long removes(Collection<? extends Object> keys);

    long removes(Object... keys);


    /**
     * 当前缓存大小
     * @return
     */
    int size();

    /**
     * 清空map
     */
    void clear();


    /**
     * 获取当前缓存 的所有key值，直接从redis中获取
     * @return
     */
    Set<Object> keySet();

    String getCacheName();

    /**
     * 获取当前 读 reids的名称
     * @return
     */
    String getReadSourceName();

    /**
     * 获取当前 写 reids的名称
     * @return
     */
    String getWriteSourceName();

}
