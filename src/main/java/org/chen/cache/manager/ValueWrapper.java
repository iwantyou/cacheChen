//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.chen.cache.manager;

import lombok.Getter;

import java.io.Serializable;

/**
 * @author: allen.chen
 * @date: 2017/8/8
 *
 */
public final class ValueWrapper implements Serializable {

    @Getter
    private final Object value;
    /**
     * 缓存结束时间,单位：毫秒
     */
    @Getter
    private long endTime;

    public ValueWrapper(Object value) {
        this.value = value;
    }

    public ValueWrapper(Object value, long cacheTime) {
        this.value = value;
        //0代表 永久存储
        this.endTime = cacheTime > 0 ? (System.currentTimeMillis() + cacheTime) : 0;
    }
}
