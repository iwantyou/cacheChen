package org.chen.cache;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.chen.cache.cacheOperation.CacheOperationInvoker;
import org.chen.cache.manager.Cache;
import org.chen.cache.manager.ValueWrapper;

/**
 * @author: allen.chen
 * @date: 2017/7/9
 * <p>
 * 默认的 Cacheable 方法处理器
 */
public class DefaultCacheableMethodHolder extends AbstractMethodHolder {


    /**
     * 批量获取
     *
     * @param keys           方法第一个参数{@link CacheOperationInvoker#args}[0]
     * @param context
     * @return
     * @throws Throwable
     */
    @Override
    public Object batch(Collection<Object> keys, MethodHolderContext context) throws Throwable {
        Class<?> returnType = context.getInvoker().getMethod().getReturnType();
        HashMap<Object, Object> cacheResult = context.getCache().getMap(keys);
        // 缓存全命中
        if (cacheResult.size() >= keys.size()) {
            Object result = getResultByReturnType(cacheResult, returnType, keys);
            return result;
        }
        // 部分命中，需要剥离没命中的key再次查询
        Collection<Object> leftKeys = keys.getClass().newInstance();
        for (Object key : keys) {
            if (!cacheResult.containsKey(key)) {
                leftKeys.add(key);
            }
        }
        BatchKeyWrapper batchKeyWrapper = new BatchKeyWrapper(keys,leftKeys);
        InvokeResultWrapper invokeResultWrapper = this.doInvokeAndPutCache(batchKeyWrapper,context);
        Map<Object, Object> noCacheResult = invokeResultWrapper.getNoCacheResult();
        Map<Object, Object> all = new HashMap<Object, Object>(cacheResult.size() + noCacheResult.size());
        all.putAll(noCacheResult);
        all.putAll(cacheResult);
        Object result = getResultByReturnType(all, returnType, keys);

        return result;
    }

    /**
     * 单个查询
     */
    @Override
    protected Object single(Object key, MethodHolderContext context) throws Throwable {
        Cache cache = context.getCache();
        ValueWrapper valueWrapper = cache.getValueWrapper(key);
        if (valueWrapper != null) {
            return valueWrapper.getValue();
        }
        Object result = context.getInvoker().invokeStoreReturnObject();
       this.doPutCache(new InvokeResultWrapper(key,result),context);
        return result;
    }

    ;

}
