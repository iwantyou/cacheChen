/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.chen.cache.expression;

import org.chen.cache.CacheExpressionRootObject;
import org.chen.cache.MethodMetadata;
import org.chen.cache.cacheOperation.CacheOperation;
import org.chen.cache.cacheOperation.CacheOperationContexts.CacheOperationContext;
import org.chen.cache.cacheOperation.CachePutOperation;
import org.chen.cache.cacheOperation.CacheableOperation;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.expression.AnnotatedElementKey;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.context.expression.MethodBasedEvaluationContext;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Utility class handling the SpEL expression parsing.
 * Meant to be used as a reusable, thread-safe component.
 * <p>
 * <p>Performs internal caching for performance reasons
 * using {@link AnnotatedElementKey}.
 *
 * @author Costin Leau
 * @author Phillip Webb
 * @author Sam Brannen
 * @author Stephane Nicoll
 * @since 3.1
 * edited by allen.chen at 2017-07-06 基于spring 源码修改
 */
public class CacheOperationExpressionEvaluator implements ExpressionEvaluator {

    private final Map<ExpressionKey, Expression> keyCache = new ConcurrentHashMap<ExpressionKey, Expression>(64);

    private final Map<ExpressionKey, Expression> conditionCache = new ConcurrentHashMap<ExpressionKey, Expression>(64);

    private final Map<ExpressionKey, Expression> unlessCache = new ConcurrentHashMap<ExpressionKey, Expression>(64);
    private final Map<AnnotatedElementKey, Method> targetMethodCache =
            new ConcurrentHashMap<AnnotatedElementKey, Method>(64);

    private final SpelExpressionParser parser;
    private final ParameterNameDiscoverer parameterNameDiscoverer;

    private final BeanFactory beanFactory;

    public CacheOperationExpressionEvaluator(BeanFactory beanFactory) {
        this.parser = new SpelExpressionParser();
        this.parameterNameDiscoverer = new DefaultParameterNameDiscoverer();
        this.beanFactory = beanFactory;
    }

    /**
     * 创建spring Spel 表达式 Context
     */
    private EvaluationContext createEvaluationContext(CacheOperationContext cacheOperationContext) {
        Method method = cacheOperationContext.getMethodMetadata().getMethod();
        Object[] args = cacheOperationContext.getMethodMetadata().getArgs();
        Object target = cacheOperationContext.getMethodMetadata().getTarget();
        Class<?> targetClass = cacheOperationContext.getMethodMetadata().getTargetClass();
        CacheExpressionRootObject rootObject = new CacheExpressionRootObject(method, args, target, targetClass);
        Method targetMethod = getTargetMethod(targetClass, method);

        //创建spring表达式Context对象
        MethodBasedEvaluationContext context = new MethodBasedEvaluationContext(rootObject, targetMethod, args, this.parameterNameDiscoverer);

        if (beanFactory != null) {
            context.setBeanResolver(new BeanFactoryResolver(beanFactory));
        }
        return context;
    }

    /**
     * 回调 表达式 接口
     */
    private abstract class ExpressionProvider {
        abstract Map<ExpressionKey, Expression> getExpressionCacheMap();

        Expression getExpression(String strExpression, MethodMetadata methodMetadata) {
            AnnotatedElementKey methodKey = new AnnotatedElementKey(methodMetadata.getMethod(), methodMetadata.getTargetClass());
            Map<ExpressionKey, Expression> expressionCacheMap = this.getExpressionCacheMap();
            ExpressionKey expressionKey = new ExpressionKey(methodKey, strExpression);
            Expression expr = expressionCacheMap.get(expressionKey);
            if (expr == null) {
                expr = CacheOperationExpressionEvaluator.this.parser.parseExpression(strExpression);
                expressionCacheMap.put(expressionKey, expr);
            }
            return expr;
        }
    }

    @Override
    public Object key(CacheOperationContext cacheOperationContext) {

        // 配置了KeyGenerator ，生成key 优先使用自定义的 KeyGenerator
        if (cacheOperationContext.getCacheOperationMetadata().getKeyGenerator() != null) {
            Object target = cacheOperationContext.getMethodMetadata().getTarget();
            Method method = cacheOperationContext.getMethodMetadata().getMethod();
            Object[] args = cacheOperationContext.getMethodMetadata().getArgs();
            return cacheOperationContext.getCacheOperationMetadata().getKeyGenerator().generate(target,method,args);
        }

        ExpressionProvider provider = new ExpressionProvider() {
            @Override
            Map<ExpressionKey, Expression> getExpressionCacheMap() {
                return CacheOperationExpressionEvaluator.this.keyCache;
            }
        };
        Expression expression = provider.getExpression(cacheOperationContext.getCacheOperationMetadata().getOperation().getKey(), cacheOperationContext.getMethodMetadata());
        EvaluationContext context = createEvaluationContext(cacheOperationContext);
        return expression.getValue(context);
    }

    @Override
    public boolean condition(CacheOperationContext cacheOperationContext) {
        //默认没有配置返回true
        if (StringUtils.isEmpty(cacheOperationContext.getCacheOperationMetadata().getOperation().getCondition())) {
            return true;
        }
        ExpressionProvider provider = new ExpressionProvider() {
            @Override
            Map<ExpressionKey, Expression> getExpressionCacheMap() {
                return CacheOperationExpressionEvaluator.this.conditionCache;
            }
        };
        Expression expression = provider.getExpression(cacheOperationContext.getCacheOperationMetadata().getOperation().getCondition(), cacheOperationContext.getMethodMetadata());
        EvaluationContext context = createEvaluationContext(cacheOperationContext);
        return expression.getValue(context, boolean.class);
    }

    @Override
    public boolean unless(CacheOperationContext cacheOperationContext) {
        CacheOperation cacheOperation = cacheOperationContext.getCacheOperationMetadata().getOperation();
        String unless = null;
        if(cacheOperation instanceof CacheableOperation){
            CacheableOperation cacheableOperation = (CacheableOperation) cacheOperation;
            unless = cacheableOperation.getUnless();
        }else if(cacheOperation instanceof CachePutOperation){
            CachePutOperation cachePutOperation = (CachePutOperation) cacheOperation;
            unless = cachePutOperation.getUnless();
        }
        if(StringUtils.hasText(unless)){
            ExpressionProvider provider = new ExpressionProvider() {
                @Override
                Map<ExpressionKey, Expression> getExpressionCacheMap() {
                    return CacheOperationExpressionEvaluator.this.unlessCache;
                }
            };
            Expression expression = provider.getExpression(unless, cacheOperationContext.getMethodMetadata());
            EvaluationContext context = createEvaluationContext(cacheOperationContext);
            return expression.getValue(context, boolean.class);
        }
        return false;



    }

    /**
     * Clear all caches.
     */
    public void clear() {
        this.keyCache.clear();
        this.conditionCache.clear();
        this.unlessCache.clear();
        this.targetMethodCache.clear();
    }

    private Method getTargetMethod(Class<?> targetClass, Method method) {
        AnnotatedElementKey methodKey = new AnnotatedElementKey(method, targetClass);
        Method targetMethod = this.targetMethodCache.get(methodKey);
        if (targetMethod == null) {
            targetMethod = AopUtils.getMostSpecificMethod(method, targetClass);
            if (targetMethod == null) {
                targetMethod = method;
            }
            this.targetMethodCache.put(methodKey, targetMethod);
        }
        return targetMethod;
    }


}
