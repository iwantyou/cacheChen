package org.chen.cache.test;

import org.chen.example.User;
import org.junit.Test;
import org.springframework.util.SerializationUtils;
import redis.clients.jedis.Jedis;

import java.util.Arrays;

/**
 * @author: allen.chen
 * @date: 2017/7/22
 */
public class JedisTest {

    @Test
    public void test(){
        //连接redis服务器
        Jedis jedis = new Jedis("192.168.0.118", 6379);


        jedis.auth("chen");
//        jedis.set("hell","hello145");
        String ping = jedis.ping();
        System.out.println(ping);
        byte[] bytes = jedis.get("default_1".getBytes());
        Object deserialize = SerializationUtils.deserialize(bytes);
        User user = (User) deserialize;
        System.out.println(deserialize);
        System.out.println(Arrays.toString(bytes));
    }

    @Test
    public void test1(){
        byte [] bytes = {-84, -19, 0, 5, 115, 114, 0, 21, 111, 114, 103, 46, 99, 104, 101, 110, 46, 101, 120, 97, 109, 112, 108, 101, 46, 85, 115, 101, 114, 26, -5, -69, 42, -42, -112, -57, -125, 2, 0, 4, 76, 0, 5, 101, 109, 97, 105, 108, 116, 0, 18, 76, 106, 97, 118, 97, 47, 108, 97, 110, 103, 47, 83, 116, 114, 105, 110, 103, 59, 76, 0, 2, 105, 100, 116, 0, 19, 76, 106, 97, 118, 97, 47, 108, 97, 110, 103, 47, 73, 110, 116, 101, 103, 101, 114, 59, 76, 0, 4, 110, 97, 109, 101, 113, 0, 126, 0, 1, 76, 0, 5, 112, 104, 111, 110, 101, 113, 0, 126, 0, 1, 120, 112, 112, 115, 114, 0, 17, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 73, 110, 116, 101, 103, 101, 114, 18, -30, -96, -92, -9, -127, -121, 56, 2, 0, 1, 73, 0, 5, 118, 97, 108, 117, 101, 120, 114, 0, 16, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 78, 117, 109, 98, 101, 114, -122, -84, -107, 29, 11, -108, -32, -117, 2, 0, 0, 120, 112, 0, 0, 0, 1, 116, 0, 6, -26, -75, -117, -24, -81, -107, 112};
        Object deserialize = SerializationUtils.deserialize(bytes);
        User user = (User) deserialize;
        System.out.println(deserialize);
    }
}
