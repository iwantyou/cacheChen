/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.chen.cache;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.chen.cache.annotation.CacheConfig;
import org.chen.cache.annotation.CacheEvict;
import org.chen.cache.annotation.CachePut;
import org.chen.cache.annotation.Cacheable;
import org.chen.cache.annotation.Caching;
import org.chen.cache.cacheOperation.CacheEvictOperation;
import org.chen.cache.cacheOperation.CacheOperation;
import org.chen.cache.cacheOperation.CachePutOperation;
import org.chen.cache.cacheOperation.CacheableOperation;
import org.chen.cache.manager.CacheChen;
import org.springframework.context.expression.AnnotatedElementKey;
import org.springframework.core.BridgeMethodResolver;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.util.ClassUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

/**
 * BaseCache implementation for parsing Spring's {@link Caching}, {@link Cacheable},
 * {@link CacheEvict}, and {@link CachePut} annotations.
 *
 * @author Costin Leau
 * @author Juergen Hoeller
 * @author Chris Beams
 * @author Phillip Webb
 * @author Stephane Nicoll
 * @author Sam Brannen
 * @since 3.1
 */
@SuppressWarnings("serial")
public class DefaultCacheAnnotationParser implements CacheAnnotationParser, Serializable {

    /**
     * Cache of CacheOperations, keyed by method on a specific target class.
     */
    private final Map<AnnotatedElementKey, Collection<CacheOperation>> methodOperationCache =
            new ConcurrentHashMap<>(1024);

    /**
     * 是否只解析public方法
     */
    private final boolean publicMethodsOnly;

    public DefaultCacheAnnotationParser() {
        this(true);
    }

    public DefaultCacheAnnotationParser(boolean publicMethodsOnly) {
        this.publicMethodsOnly = publicMethodsOnly;
    }

    /**
     * 获取所有的 CacheOperation
     * 每种缓存注解对应一个CacheOperation对象
     *
     * @param method
     * @param targetClass
     * @return
     */
    public Collection<CacheOperation> getAllCacheOperations(Method method, Class<?> targetClass) {
        AnnotatedElementKey methodClassKey = new AnnotatedElementKey(method, targetClass);
        Collection<CacheOperation> cached = methodOperationCache.get(methodClassKey);
        if (cached != null) {
            return cached;
        }
        Collection<CacheOperation> cacheOps = computeCacheOperations(method, targetClass);
        if (cacheOps != null) {
            this.methodOperationCache.put(methodClassKey, cacheOps);
            return cacheOps;
        }
        return new ArrayList<>();
    }

    private Collection<CacheOperation> computeCacheOperations(Method method, Class<?> targetClass) {
        // Don't allow no-public methods as required.
        if (publicMethodsOnly && !Modifier.isPublic(method.getModifiers())) {
            return null;
        }

        // The method may be on an interface, but we need attributes from the target class.
        // If the target class is null, the method will be unchanged.
        Method specificMethod = ClassUtils.getMostSpecificMethod(method, targetClass);
        // If we are dealing with method with generic parameters, find the original method.
        specificMethod = BridgeMethodResolver.findBridgedMethod(specificMethod);

        // First try is the method in the target class.
        Collection<CacheOperation> opDef = this.parseCacheAnnotations(specificMethod);
        if (opDef != null) {
            return opDef;
        }

        // Second try is the caching operation on the target class.
        opDef = this.parseCacheAnnotations(specificMethod.getDeclaringClass());
        if (opDef != null && ClassUtils.isUserLevelMethod(method)) {
            return opDef;
        }

        if (specificMethod != method) {
            // Fallback is to look at the original method.
            opDef = this.parseCacheAnnotations(method);
            if (opDef != null) {
                return opDef;
            }
            // Last fallback is the class of the original method.
            opDef = this.parseCacheAnnotations(method.getDeclaringClass());
            if (opDef != null && ClassUtils.isUserLevelMethod(method)) {
                return opDef;
            }
        }

        return null;
    }

    @Override
    public Collection<CacheOperation> parseCacheAnnotations(Class<?> type) {
        DefaultCacheConfig defaultConfig = getDefaultCacheConfig(type);
        return parseCacheAnnotations(defaultConfig, type);
    }

    @Override
    public Collection<CacheOperation> parseCacheAnnotations(Method method) {
        DefaultCacheConfig defaultConfig = getDefaultCacheConfig(method.getDeclaringClass());
        return parseCacheAnnotations(defaultConfig, method);
    }

    protected Collection<CacheOperation> parseCacheAnnotations(DefaultCacheConfig cachingConfig, AnnotatedElement ae) {
        Collection<CacheOperation> ops = null;

        Collection<Cacheable> cacheables = AnnotatedElementUtils.getAllMergedAnnotations(ae, Cacheable.class);
        if (!cacheables.isEmpty()) {
            ops = lazyInit(ops);
            for (Cacheable cacheable : cacheables) {
                ops.add(parseCacheableAnnotation(ae, cachingConfig, cacheable));
            }
        }
        Collection<CacheEvict> evicts = AnnotatedElementUtils.getAllMergedAnnotations(ae, CacheEvict.class);
        if (!evicts.isEmpty()) {
            ops = lazyInit(ops);
            for (CacheEvict evict : evicts) {
                ops.add(parseEvictAnnotation(ae, cachingConfig, evict));
            }
        }
        Collection<CachePut> puts = AnnotatedElementUtils.getAllMergedAnnotations(ae, CachePut.class);
        if (!puts.isEmpty()) {
            ops = lazyInit(ops);
            for (CachePut put : puts) {
                ops.add(parsePutAnnotation(ae, cachingConfig, put));
            }
        }
        Collection<Caching> cachings = AnnotatedElementUtils.getAllMergedAnnotations(ae, Caching.class);
        if (!cachings.isEmpty()) {
            ops = lazyInit(ops);
            for (Caching caching : cachings) {
                Collection<CacheOperation> cachingOps = parseCachingAnnotation(ae, cachingConfig, caching);
                if (cachingOps != null) {
                    ops.addAll(cachingOps);
                }
            }
        }

        return ops;
    }

    private <T extends Annotation> Collection<CacheOperation> lazyInit(Collection<CacheOperation> ops) {
        return (ops != null ? ops : new ArrayList<CacheOperation>(1));
    }

    CacheableOperation parseCacheableAnnotation(AnnotatedElement ae, DefaultCacheConfig defaultConfig, Cacheable cacheable) {
        CacheableOperation cacheableOperation = new CacheableOperation();
        cacheableOperation.setName(ae.toString());
        this.setDefaultConfig(cacheableOperation,
                defaultConfig,
                cacheable.cacheNames(),
                cacheable.keyGenerator(),
                cacheable.readSourceName(),
                cacheable.writeSourceName());
        if (cacheable.cacheNull()) {
            if (cacheable.sync()) {
                throw new IllegalStateException("@Cacheable(cacheNull=true) 不支持 sync  配置");
            }
            if (cacheable.batch()) {
                throw new IllegalStateException("@Cacheable(cacheNull=true) 不支持 batch  配置");
            }
        }
        if (cacheable.sync()) {
            if (cacheable.cacheNames().length > 1) {
                throw new IllegalStateException("@Cacheable(sync=true) 只能配置一个cacheName");
            }
            if (StringUtils.hasText(cacheable.unless())) {
                throw new IllegalStateException("@Cacheable(sync=true) 不支持 unless  配置");
            }
        }
        if (cacheable.slide()) {
            if (cacheable.cacheNames().length > 1) {
                throw new IllegalStateException("@Cacheable(slide=true) 只能配置一个 cacheName");
            }
            if (StringUtils.hasText(cacheable.unless())) {
                throw new IllegalStateException("@Cacheable(slide=true) 不支持 unless  配置");
            }
            if (cacheable.sync()) {
                throw new IllegalStateException("@Cacheable(slide=true) 不支持 sync  配置");
            }
        }

        if (StringUtils.hasText(cacheable.resultCacheKey()) && !cacheable.batch()) {
            throw new IllegalStateException("配置错误,配置了resultCacheKey，必须配置batch，系统将走批量处理缓存的逻辑");
        }

        cacheableOperation.setCondition(cacheable.condition());
        cacheableOperation.setUnless(cacheable.unless());
        cacheableOperation.setKey(cacheable.key());
        cacheableOperation.setMethodHolder(cacheable.methodHolder());
        cacheableOperation.setSync(cacheable.sync());
        cacheableOperation.setResultCacheKey(cacheable.resultCacheKey());
        cacheableOperation.setCacheTime(cacheable.cacheTime());
        cacheableOperation.setSlide(cacheable.slide());
        cacheableOperation.setCacheNull(cacheable.cacheNull());
        cacheableOperation.setBatch(cacheable.batch());

        validateCacheOperation(ae, cacheableOperation);

        return cacheableOperation;
    }

    CachePutOperation parsePutAnnotation(AnnotatedElement ae, DefaultCacheConfig defaultConfig, CachePut cachePut) {
        CachePutOperation cachePutOperation = new CachePutOperation();

        cachePutOperation.setName(ae.toString());
        this.setDefaultConfig(cachePutOperation,
                defaultConfig,
                cachePut.cacheNames(),
                cachePut.keyGenerator(),
                null,
                cachePut.writeSourceName());
        cachePutOperation.setCondition(cachePut.condition());
        cachePutOperation.setUnless(cachePut.unless());
        cachePutOperation.setKey(cachePut.key());

        cachePutOperation.setMethodHolder(cachePut.methodHolder());
        cachePutOperation.setResultCacheKey(cachePut.resultCacheKey());
        cachePutOperation.setCacheTime(cachePut.cacheTime());
        cachePutOperation.setCacheNull(cachePut.cacheNull());
        cachePutOperation.setBatch(cachePut.batch());

        validateCacheOperation(ae, cachePutOperation);

        return cachePutOperation;
    }

    CacheEvictOperation parseEvictAnnotation(AnnotatedElement ae, DefaultCacheConfig defaultConfig, CacheEvict cacheEvict) {
        CacheEvictOperation cacheEvictOperation = new CacheEvictOperation();

        cacheEvictOperation.setName(ae.toString());
        this.setDefaultConfig(cacheEvictOperation,
                defaultConfig,
                cacheEvict.cacheNames(),
                cacheEvict.keyGenerator(),
                null,
                cacheEvict.writeSourceName());

        cacheEvictOperation.setCondition(cacheEvict.condition());
        cacheEvictOperation.setKey(cacheEvict.key());
        cacheEvictOperation.setKeyGenerator(cacheEvict.keyGenerator());
        cacheEvictOperation.setCacheWide(cacheEvict.allEntries());
        cacheEvictOperation.setBeforeInvocation(cacheEvict.beforeInvocation());

        validateCacheOperation(ae, cacheEvictOperation);

        return cacheEvictOperation;
    }

    private void setDefaultConfig(CacheOperation cacheOperation,
                                  DefaultCacheConfig defaultConfig,
                                  String[] cacheNames,
                                  String keyGenerator,
                                  String readSourceName,
                                  String writeSourceName) {

        if (defaultConfig.cacheNames.length == 0 && cacheNames.length == 0) {
            cacheOperation.setCacheName(CacheChen.DEFAULT_CACHE_NAME);
        } else if (cacheNames.length >= 0) {
            cacheOperation.setCacheNames(cacheNames);
        } else {
            cacheOperation.setCacheNames(defaultConfig.cacheNames);
        }

        if (StringUtils.hasText(keyGenerator)) {
            cacheOperation.setKeyGenerator(keyGenerator);
        } else {
            cacheOperation.setKeyGenerator(defaultConfig.keyGenerator);
        }

        if (StringUtils.hasText(readSourceName)) {
            cacheOperation.setReadSourceName(readSourceName);
        } else {
            cacheOperation.setReadSourceName(defaultConfig.readSourceName);
        }

        if (StringUtils.hasText(writeSourceName)) {
            cacheOperation.setWriteSourceName(writeSourceName);
        } else {
            cacheOperation.setWriteSourceName(defaultConfig.writeSourceName);
        }

    }

    Collection<CacheOperation> parseCachingAnnotation(AnnotatedElement ae, DefaultCacheConfig defaultConfig, Caching caching) {
        Collection<CacheOperation> ops = null;

        Cacheable[] cacheables = caching.cacheable();
        if (!ObjectUtils.isEmpty(cacheables)) {
            ops = lazyInit(ops);
            for (Cacheable cacheable : cacheables) {
                if (cacheable.batch()) {
                    throw new IllegalStateException("配置错误,不能在组合注解@Caching中配置 @Cacheable(batch=true)");
                }
                if (cacheable.sync()) {
                    throw new IllegalStateException("配置错误,不能在组合注解@Caching中配置 @Cacheable(sync=true)");
                }
                if (cacheable.slide()) {
                    throw new IllegalStateException("配置错误,不能在组合注解@Caching中配置 @Cacheable(slide=true)");
                }

                ops.add(parseCacheableAnnotation(ae, defaultConfig, cacheable));
            }

        }
        CacheEvict[] cacheEvicts = caching.evict();
        if (!ObjectUtils.isEmpty(cacheEvicts)) {
            ops = lazyInit(ops);
            for (CacheEvict cacheEvict : cacheEvicts) {
                ops.add(parseEvictAnnotation(ae, defaultConfig, cacheEvict));
            }
        }
        CachePut[] cachePuts = caching.put();
        if (!ObjectUtils.isEmpty(cachePuts)) {
            ops = lazyInit(ops);
            for (CachePut cachePut : cachePuts) {
                ops.add(parsePutAnnotation(ae, defaultConfig, cachePut));
            }
        }

        return ops;
    }

    /**
     * Provides the {@link DefaultCacheConfig} instance for the specified {@link Class}.
     *
     * @param target the class-level to handle
     * @return the default config (never {@code null})
     */
    DefaultCacheConfig getDefaultCacheConfig(Class<?> target) {
        CacheConfig annotation = AnnotatedElementUtils.getMergedAnnotation(target, CacheConfig.class);
        if (annotation != null) {
            return new DefaultCacheConfig(annotation.cacheNames(), annotation.keyGenerator(), annotation.readSourceName(), annotation.writeSourceName());
        }
        return new DefaultCacheConfig();
    }

    /**
     * Validates the specified {@link CacheOperation}.
     * <p>Throws an {@link IllegalStateException} if the state of the operation is
     * invalid. As there might be multiple sources for default values, this ensure
     * that the operation is in a proper state before being returned.
     *
     * @param ae        the annotated element of the cache operation
     * @param operation the {@link CacheOperation} to validate
     */
    private void validateCacheOperation(AnnotatedElement ae, CacheOperation operation) {
        if (StringUtils.hasText(operation.getKey()) && StringUtils.hasText(operation.getKeyGenerator())) {
            throw new IllegalStateException("Invalid cache annotation configuration on '" +
                    ae.toString() + "'. Both 'key' and 'keyGenerator' attributes have been set. " +
                    "These attributes are mutually exclusive: either set the SpEL expression used to" +
                    "compute the key at runtime or set the name of the KeyGenerator bean to use.");
        }
        if (operation instanceof CacheableOperation) {
            CacheableOperation cacheableOperation = (CacheableOperation) operation;
            if (StringUtils.hasText(cacheableOperation.getKey()) && StringUtils.hasText(cacheableOperation.getResultCacheKey())) {
                throw new IllegalStateException("参数配置错误-key:" + cacheableOperation.getKey() + "  resultCacheKey:" + cacheableOperation.getResultCacheKey() + " 不能同时配置");
            }
        }

    }

    /**
     * Provides default settings for a given set of cache operations.
     */
    static class DefaultCacheConfig {

        private final String[] cacheNames;

        /**
         * 读的 redis 库名
         */
        private final String readSourceName;

        /**
         * 写的 redis 库名
         */
        private final String writeSourceName;

        private final String keyGenerator;

        public DefaultCacheConfig() {
            this(new String[]{}, null, null, null);
        }

        private DefaultCacheConfig(String[] cacheNames, String keyGenerator, String readSourceName, String writeSourceName) {
            this.cacheNames = cacheNames;
            this.keyGenerator = keyGenerator;
            this.readSourceName = readSourceName;
            this.writeSourceName = writeSourceName;
        }

    }


}
