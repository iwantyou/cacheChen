package org.chen.cache;

import java.util.Collection;
import java.util.Map;

/**
 * @author: Allen
 * @date: 2017/7/9
 *  默认的 CachePut 方法处理器
 */
public class DefaultCachePutMethodHolder extends AbstractMethodHolder{


    /**
     * 批量获取
     *
     * @param keys           方法第一个参数
     * @param context
     * @return
     * @throws Throwable
     */
    @Override
    public Object batch(Collection<Object> keys, MethodHolderContext context) throws Throwable {
        Class<?> returnType = context.getInvoker().getMethod().getReturnType();

        BatchKeyWrapper batchKeyWrapper = new BatchKeyWrapper(keys);
        InvokeResultWrapper invokeResultWrapper = this.doInvokeAndPutCache(batchKeyWrapper,context);
        Map<Object, Object> noCacheResult = invokeResultWrapper.getNoCacheResult();

        Object result = getResultByReturnType(noCacheResult, returnType, keys);

        return result;
    }



    /**
     * 单个查询
     */
    @Override
    protected Object single(Object key, MethodHolderContext context) throws Throwable {
        Object result = context.getInvoker().invokeStoreReturnObject();
        this.doPutCache(new InvokeResultWrapper(key,result),context);
        return result;
    }

}
