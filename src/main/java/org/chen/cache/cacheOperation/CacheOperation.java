/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.chen.cache.cacheOperation;

import lombok.Data;
import lombok.ToString;
import org.springframework.cache.interceptor.BasicOperation;
import org.springframework.util.Assert;

import java.lang.reflect.AnnotatedElement;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Base class for cache operations.
 *
 * @author Costin Leau
 * @author Stephane Nicoll
 * @author Marcin Kamionowski
 * @since 3.1
 */
@ToString
@Data
public abstract class CacheOperation implements BasicOperation {

    /**
     * 设置的值: {@link AnnotatedElement#toString()}
     */
    private String name;

    private Set<String> cacheNames;

    private String key;

    private String keyGenerator;

    private String condition;

    /**
     * 是否缓存null值
     *
     * @return
     */
    private boolean cacheNull;

    private String resultCacheKey;

    /**
     * 读的 redis 库名
     */
    private String readSourceName;
    /**
     * 写的 redis 库名
     */
    private String writeSourceName;

    private int cacheTime;

    private boolean batch;

    public void setCacheName(String cacheName) {
        Assert.hasText(cacheName, "Cache name must not be empty");
        this.cacheNames = Collections.singleton(cacheName);
    }

    public void setCacheNames(String... cacheNames) {
        this.cacheNames = new LinkedHashSet<String>(cacheNames.length);
        for (String cacheName : cacheNames) {
            Assert.hasText(cacheName, "Cache name must be non-empty if specified");
            this.cacheNames.add(cacheName);
        }
    }
    /**
     * This implementation compares the {@code toString()} results.
     * @see #toString()
     */
    @Override
    public boolean equals(Object other) {
        return (other instanceof CacheOperation && toString().equals(other.toString()));
    }

    /**
     * @see #toString()
     * @return
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

}
