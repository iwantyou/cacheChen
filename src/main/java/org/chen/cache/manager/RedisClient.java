package org.chen.cache.manager;

import org.springframework.util.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: allen.chen
 * @date: 2017/7/8
 */
public final class RedisClient {

    /**
     * redis 客户端，单例
     */
    private static RedisClient redisClient;

    /**
     * redis 配置
     */
    private final RedisConfig redisConfig;

    /**
     * redis 连接池
     * key: redis名字
     * value:redis连接池
     */
    private Map<String, JedisPool> redisPools = new HashMap<String, JedisPool>();


    /**
     * 初始化redis 配置
     *
     * @param redisConfig
     *
     * @see CacheManager#init 可通过 CacheManager 初始化，也可以自定义，建议使用 CacheManager（此类为单例，一般是spring bean）
     */
    public RedisClient(RedisConfig redisConfig) {
        if (redisClient != null) {
            throw new IllegalStateException("redis 全局配置只能创建一次");
        }
        redisClient = this;
        this.redisConfig = redisConfig;
        this.initPool();

    }

    /**
     * 获取配置
     * @return
     */
    public static RedisClient getInstance(){
        if(redisClient == null){
            throw new IllegalStateException(RedisClient.class.getName() + " 实例configuration未初始化");
        }
        return redisClient;
    }

    /**
     * 初始化 redis 连接池
     */
    private void initPool() {

        for (int i = 0; i < redisConfig.getSources().length; i++) {
            RedisSource source = redisConfig.getSources()[i];
            if (!StringUtils.hasText(source.getSourceName())) {
                throw new InvalidParameterException("参数配置错误，redis名字 sourceName 不能为空");
            }
            if (redisPools.containsKey(source.getSourceName())) {
                throw new InvalidParameterException("参数配置错误，redis名字 " + source.getSourceName() + " 重复");
            }
            int timeout = source.getTimeout();
            if (timeout <= 0) {
                timeout = 15;
            }
            JedisPool pool = new JedisPool(source.getPoolConfig(), source.getIp(), source.getPort(), timeout,
                    source.getPassword());
            redisPools.put(source.getSourceName(), pool);
        }
        if (StringUtils.isEmpty(redisConfig.getDefaultReadSourceName())) {
            throw new InvalidParameterException("defaultReadSourceName" + " 不能为空，请检测配置");
        }
        if (!redisPools.containsKey(redisConfig.getDefaultReadSourceName())) {
            throw new InvalidParameterException("defaultReadSourceName" + " 与实际配置的 sourceName 不匹配，请检测配置");
        }
        if (StringUtils.isEmpty(redisConfig.getDefaultWriteSourceName())) {
            throw new InvalidParameterException("defaultWriteSourceName" + " 不能为空，请检测配置");
        }
        if (!redisPools.containsKey(redisConfig.getDefaultWriteSourceName())) {
            throw new InvalidParameterException("defaultWriteSourceName" + " 与实际配置的 sourceName 不匹配，请检测配置");
        }

    }

    /**
     * 通过Jedis pool 获取Jedis
     * 注意：获取Jedis 实例,使用完之后 必须 关闭 {@link Jedis#close()}
     *
     * @param sourceName
     * @return
     */
    public Jedis getJedis(String sourceName) {
        JedisPool pool = redisPools.get(sourceName);
        if(pool == null){
            throw new InvalidParameterException("sourceName:" + sourceName +" 没有配置，请检查");
        }
        Jedis resource = pool.getResource();
        return resource;
    }


    /**
     * 通过Jedis pool 获取Jedis
     * 注意：获取Jedis 实例,使用完之后 必须 关闭 {@link Jedis#close()}
     *
     * @return
     */
    public Jedis getDefaultWriteJedis() {
        JedisPool pool = redisPools.get(redisConfig.getDefaultWriteSourceName());
        Jedis resource = pool.getResource();
        return resource;
    }

    /**
     * 通过Jedis pool 获取Jedis
     * 注意：获取Jedis 实例,使用完之后 必须 关闭 {@link Jedis#close()}
     *
     * @return
     */
    public Jedis getDefaultReadJedis() {
        JedisPool pool = redisPools.get(redisConfig.getDefaultReadSourceName());
        Jedis resource = pool.getResource();
        return resource;
    }

    public RedisConfig getRedisConfig() {
        return redisConfig;
    }
}
