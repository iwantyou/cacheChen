package org.chen.cache.cacheOperation;

import org.chen.cache.AbstractMethodHolder;
import org.chen.cache.manager.Cache;
import lombok.Getter;
import org.springframework.cache.interceptor.KeyGenerator;

import java.util.Map;

/**
 * @author: allen.chen
 * @date: 2017/7/23
 */
public class CacheOperationMetadata {

    @Getter
    private final CacheOperation operation;

    @Getter
    private final KeyGenerator keyGenerator;
    @Getter
    private final AbstractMethodHolder methodHolder;

    @Getter
    private final Map<String, Cache> caches;

    public CacheOperationMetadata(CacheOperation operation, Map<String, Cache> caches, KeyGenerator keyGenerator, AbstractMethodHolder methodHolder) {
        this.operation = operation;
        this.keyGenerator = keyGenerator;
        this.methodHolder = methodHolder;
        this.caches = caches;
    }
}
