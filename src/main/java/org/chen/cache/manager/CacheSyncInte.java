package org.chen.cache.manager;

/**
 * @author: allen.chen
 * @date: 2017/7/22
 */
interface CacheSyncInte {

    /**
     * Acquires the proper read lock for a given cache key
     *
     * @param key - The key that retrieves a value that you want to protect via locking
     */
    void acquireReadLockOnKey(Object key);

    /**
     * Acquires the proper write lock for a given cache key
     *
     * @param key - The key that retrieves a value that you want to protect via locking
     */
    void acquireWriteLockOnKey(Object key);

    /**
     * Try to get a read lock on a given key. If can't get it in timeout millis then
     * return a boolean telling that it didn't get the lock
     *
     * @param key - The key that retrieves a value that you want to protect via locking
     * @param timeout - millis until giveup on getting the lock
     * @return whether the lock was awarded
     * @throws InterruptedException
     */
    boolean tryReadLockOnKey(Object key, long timeout) throws InterruptedException;

    /**
     * Try to get a write lock on a given key. If can't get it in timeout millis then
     * return a boolean telling that it didn't get the lock
     *
     * @param key - The key that retrieves a value that you want to protect via locking
     * @param timeout - millis until giveup on getting the lock
     * @return whether the lock was awarded
     * @throws InterruptedException
     */
    boolean tryWriteLockOnKey(Object key, long timeout) throws InterruptedException;

    /**
     * Release a held read lock for the passed in key
     *
     * @param key - The key that retrieves a value that you want to protect via locking
     */
    void releaseReadLockOnKey(Object key);

    /**
     * Release a held write lock for the passed in key
     *
     * @param key - The key that retrieves a value that you want to protect via locking
     */
    void releaseWriteLockOnKey(Object key);

    /**
     * Returns true if a read lock for the key is held by the current thread
     *
     * @param key
     * @return true if a read lock for the key is held by the current thread
     * @throws UnsupportedOperationException if querying the read lock state is not supported
     */
    boolean isReadLockedByCurrentThread(Object key) throws UnsupportedOperationException;

    /**
     * Returns true if a write lock for the key is held by the current thread
     *
     * @param key
     * @return true if a write lock for the key is held by the current thread
     * @throws UnsupportedOperationException if querying the write lock state is not supported
     */
    boolean isWriteLockedByCurrentThread(Object key) throws UnsupportedOperationException;
}
