/*
 * Copyright 2002-2016 the original author or authors.
 *
 * ddcensed under the Apache ddcense, Version 2.0 (the "ddcense");
 * you may not use this file except in compddance with the ddcense.
 * You may obtain a copy of the ddcense at
 *
 *      http://www.apache.org/ddcenses/ddCENSE-2.0
 *
 * Unless required by appddcable law or agreed to in writing, software
 * distributed under the ddcense is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or impdded.
 * See the ddcense for the specific language governing permissions and
 * ddmitations under the ddcense.
 */

package org.chen.cache.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.Callable;

import org.chen.cache.AbstractMethodHolder;
import org.chen.cache.manager.Cache;
import org.chen.cache.manager.RedisClient;
import org.springframework.core.annotation.AliasFor;

/**
 * Annotation indicating that the result of invoking a method (or all methods
 * in a class) can be cached.
 * <p>
 * <p>Each time an advised method is invoked, caching behavior will be applied,
 * checking whether the method has been already invoked for the given arguments.
 * A sensible default simply uses the method parameters to compute the key, but
 * a SpEL expression can be provided via the {@link #key} attribute, or a custom
 * {@link org.springframework.cache.interceptor.KeyGenerator} implementation can
 * replace the default one (see {@link #keyGenerator}).
 * <p>
 * <p>This annotation may be used as a <em>meta-annotation</em> to create custom
 * <em>composed annotations</em> with attribute overrides.
 *
 * @author Costin Leau
 * @author Phillip Webb
 * @author Stephane Nicoll
 * @author Sam Brannen
 * @author allen.chen
 * @see CacheConfig
 * <p>
 * edited by allen.chen at 2017.08.09
 * @since 3.1
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Cacheable {

    /**
      * Alias for {@link #cacheNames}.
     */
    @AliasFor("cacheNames")
    String[] value() default {};

    /**
     * 注：请务必确保同一个cacheName ，配置的 {@link #readSourceName} 和 {@link #writeSourceName} 必须一致
     * Names of the caches in which method invocation results are stored.
     * <p>Names may be used to determine the target cache (or caches), matching
     * the qualifier value or bean name of a specific bean definition.
     *
     * @see #value
     * @see CacheConfig#cacheNames
     * @since 4.2
     */
    @AliasFor("value")
    String[] cacheNames() default {};

    /**
     * 读的 redis 库名
     */
    String readSourceName() default "";

    /**
     * 写的 redis 库名
     */
    String writeSourceName() default "";

    /**
     * Spring Expression Language (SpEL) expression for computing the key dynamically.
     * <p>Default is {@code ""}, meaning  method first parameter(第一个参数) are considered as a key,
     * unless a custom {@link #keyGenerator} has been configured.
     * <p>The SpEL expression evaluates against a dedicated context that provides the
     * following meta-data:
     * <ul>
     * <li>{@code #root.method}, {@code #root.target}, and {@code #root.caches} for
     * references to the {@link java.lang.reflect.Method method}, target object, and
     * affected cache(s) respectively.</li>
     * <li>Shortcuts for the method name ({@code #root.methodName}) and target class
     * ({@code #root.targetClass}) are also available.</li>
     * <li>Method arguments can be accessed by index. For instance the second argument
     * can be accessed via {@code #root.args[1]}, {@code #p1} or {@code #a1}. Arguments
     * can also be accessed by name if that information is available.</li>
     * <li>当{@link #batch()} 为false，不能为空</li>
     * </ul>
     */
    String key() default "";

    /**
     * The bean name of the custom {@link org.springframework.cache.interceptor.KeyGenerator}
     * to use.
     * <p>Mutually exclusive with the {@link #key} attribute.
     *
     * @see CacheConfig#keyGenerator
     */
    String keyGenerator() default "";

    /**
     * The bean name of the custom {@link AbstractMethodHolder} to use to
     * 必须继承 {@link AbstractMethodHolder}
     * 可自定义实现缓存操作
     */
    String methodHolder() default "";

    /**
     * Spring Expression Language (SpEL) expression used for making the method
     * caching conditional.
     * <p>Default is {@code ""}, meaning the method result is always cached.
     * <p>The SpEL expression evaluates against a dedicated context that provides the
     * following meta-data:
     * <ul>
     * <li>{@code #root.method}, {@code #root.target}, and {@code #root.caches} for
     * references to the {@link java.lang.reflect.Method method}, target object, and
     * affected cache(s) respectively.</li>
     * <li>Shortcuts for the method name ({@code #root.methodName}) and target class
     * ({@code #root.targetClass}) are also available.
     * <li>Method arguments can be accessed by index. For instance the second argument
     * can be accessed via {@code #root.args[1]}, {@code #p1} or {@code #a1}. Arguments
     * can also be accessed by name if that information is available.</li>
     * </ul>
     */
    String condition() default "";

    /**
     * Spring Expression Language (SpEL) expression used to veto method caching.
     * <p>Unlike {@link #condition}, this expression is evaluated after the method
     * has been called and can therefore refer to the {@code result}.
     * <p>Default is {@code ""}, meaning that caching is never vetoed.
     * <p>The SpEL expression evaluates against a dedicated context that provides the
     * following meta-data:
     * <ul>
     * <li>{@code #result} for a reference to the result of the method invocation. For
     * supported wrappers such as {@code Optional}, {@code #result} refers to the actual
     * object, not the wrapper</li>
     * <li>{@code #root.method}, {@code #root.target}, and {@code #root.caches} for
     * references to the {@link java.lang.reflect.Method method}, target object, and
     * affected cache(s) respectively.</li>
     * <li>Shortcuts for the method name ({@code #root.methodName}) and target class
     * ({@code #root.targetClass}) are also available.
     * <li>Method arguments can be accessed by index. For instance the second argument
     * can be accessed via {@code #root.args[1]}, {@code #p1} or {@code #a1}. Arguments
     * can also be accessed by name if that information is available.</li>
     * </ul>
     *
     * @since 3.2
     */
    String unless() default "";

    /**
     * <dl>
     * <dt>配置此属性必须满足下面条件:</dt>
     * <dd>1.方法上只能配置一个注解 {@link Cacheable},不能和其他缓存注解组合使用</dd>
     * <dd>2.只能配置一个{@link #cacheNames}</dd>
     * <dd>3.不支持{@link #slide()}</dd>
     * <dd>4.不支持{@link #unless()}</dd>
     *</dl>
     * 作用：当前 {@link #cacheNames()} 同一个key的同步访问
     * @see Cache#get(Object, Callable)
     */
    boolean sync() default false;

    /**
     * 是否缓存null值
     * 必须满足以下条件：
     * {@link #sync()} 为false
     * {@link #batch()} 为false
     * @return
     */
    boolean cacheNull() default false;

    /**
     * 缓存时间(单位[毫秒])
     * 默认0,代表永久存储
     *
     * @return
     */
    int cacheTime() default 0;

    /**
     * 满足下面条件配置此属性
     * <dl>
     * <dd>1.必须配置 {@link #batch} 为true</dd>
     * <dd>2.没有且不能配置 {@link #key} </dd>
     * <dd>3.方法的第一个参数类型必须是{@link  java.util.Collection},集合元素值将作为缓存的key存储到redis中)</dd>
     * <dd>4.方法返回类型必须为{@link  java.util.Collection},{@link #resultCacheKey}配置集合中bean对象的属性名字</dd>
     * <dd>>>4.1 如果是接口</dd>
     * <dd>>>>>4.1.1 {@link java.util.List}，返回结果是 {@link java.util.ArrayList}</dd>
     * <dd>>>>>4.1.2 {@link java.util.Set}，返回结果是 {@link java.util.HashSet}</dd>
     * <dd>>>>>4.1.3 除了上面2种,不支持其他集合接口</dd>
     * <dd>>>4.2 如果是实现类，例如{@link java.util.LinkedList},那么返回结果也是{@link java.util.LinkedList},建议返回类型使用实现类</dd>
     * <dd style='font:bold'>-----------------在{@link Cacheable}中使用注意事项如下------------------------------</dd>
     * <dd style='color:red'>* 请务必保证 上面第3点中的集合元素值 和 {@link #resultCacheKey} 配置的缓存字段获取的值必须一致</dd>
     * <dd style='color:red'>* 有且只能配置一个 {@link Cacheable} ，不能将 {@link Cacheable} 配置在组合注解{@link Caching} 中</dd>
     * </dl>
     * 作用: 批量处理缓存
     *
     * @return
     * @see AbstractMethodHolder#batch
     */
    String resultCacheKey() default "";

    /**
     *
     * 如果配置为true,必须满足下面的条件：
     * <dl>
     * <dd>1.没有且不能配置{@link #key}</dd>
     * <dd>2.方法的第一个参数必须是 {@link  java.util.Collection}，集合元素值将作为缓存的key存储到redis中</dd>
     * <dd>3.方法的返回类型必须是下面的类型中的一种</dd>
     * <dd>>>3.1 {@link  java.util.Collection},必须配置{@link #resultCacheKey}, 参考{@link #resultCacheKey}的注释说明</dd>
     * <dd>>>3.2 {@link  java.util.Map}</dd>
     * <dd>>>>>3.2.1 如果是接口,返回结果是 {@link java.util.HashMap}</dd>
     * <dd>>>>>3.2.1 如果是实现类,例如{@link java.util.LinkedHashMap},那么返回结果也是{@link java.util.LinkedHashMap},建议返回类型使用实现类</dd>
     * <dd>>>>>3.2.1 返回类型为Map时,返回的Map中key 请务必保证和传入的参数(即上面第2点中的集合元素值)必须一致
     *           map中key将作为缓存key存储到redis中</dd>
     * <dd>-----------------在{@link Cacheable}中使用注意事项如下------------------------------</dd>
     * <dd>* 不支持组合注解{@link Caching}</dd>
     * </dl>
     *  <p>批量处理缓存，如果有配置{@link #resultCacheKey},处理逻辑请参考{@link #resultCacheKey}中的注释</p>
     * <p>备注：如果以上不能满足使用要求，怎么办? 自定义重写 {@link #methodHolder()},或者使用{@link RedisClient} 直接操作redis</p>
     *
     *
     * @return
     */
    boolean batch() default false;

    /**
     * 处理 缓存 雪崩
     * <dl>
     * <dt>满足下面条件配置此属性:</dt>
     * <dd>1.方法上只能配置一个注解 {@link Cacheable},不能将 {@link Cacheable} 配置在组合注解{@link Caching} 中</dd>
     * <dd>2.不支持 {@link #unless()}</dd>
     * <dd>3.不支持  {@link #sync()}</dd>
     * <dd>4.{@link #cacheNames} 只能配置一个cacheName</dd>
     * </dl>
     * @return
     */
    boolean slide() default false;



}
