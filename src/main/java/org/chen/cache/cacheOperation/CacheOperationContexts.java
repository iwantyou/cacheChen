package org.chen.cache.cacheOperation;


import org.chen.cache.AbstractMethodHolder;
import org.chen.cache.DefaultCacheAnnotationParser;
import org.chen.cache.DefaultSlideMethodHolder;
import org.chen.cache.MethodMetadata;
import org.chen.cache.annotation.CachePut;
import org.chen.cache.annotation.Cacheable;
import org.chen.cache.manager.Cache;
import org.chen.cache.manager.CacheManager;
import lombok.Getter;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 多个缓存注解 配置
 * 例如：
 * 1.配置了2个{@link Cacheable} 就生成2个CacheOperationContext
 * 2.这个类将数据整理到 {@link CacheOperationContexts#contexts} 中
 * key:{@link Class <? extends CacheOperation>}
 * value:上面key对用的一个或多个{@link CacheOperationContext}
 */
public class CacheOperationContexts {

    private final MultiValueMap<Class<? extends CacheOperation>, CacheOperationContext> contexts =
            new LinkedMultiValueMap<>();

    private final static Map<CacheOperationCacheKey, CacheOperationMetadata> metadataCache =
            new ConcurrentHashMap<>(1024);

    /**
     * 缓存 同步 标志位
     */
    @Getter
    private final boolean sync;

    /**
     * 缓存 雪崩 标志位
     */
    @Getter
    private final boolean slide;

    /**
     * 方法参数 信息
     */
    private final MethodMetadata methodMetadata;


    public CacheOperationContexts(ContextCall contextCall, MethodMetadata methodMetadata) {
        this.methodMetadata = methodMetadata;
        Collection<CacheOperation> operations = contextCall.getCacheAnnotationParser().getAllCacheOperations(methodMetadata.getMethod(), methodMetadata.getTargetClass());
        for (CacheOperation operation : operations) {
            CacheOperationMetadata metadata = this.getCacheOperationMetadata(contextCall,operation,methodMetadata.getMethod(), methodMetadata.getTargetClass());
            CacheOperationContext context = new CacheOperationContext(metadata);
            this.contexts.add(operation.getClass(), context);
        }

        this.sync = determineSyncFlag();
        this.slide = determineSlideFlag();
        if(sync && slide){
            throw new IllegalStateException("配置错误，缓存同步 sync=true 不能和缓存雪崩slide=true组合使用 ");
        }
    }

    /**
     * Return the {@link CacheOperationMetadata}
     * used for the operation.
     * @param operation the operation
     * @param method the method on which the operation is invoked
     * @param targetClass the target type
     * @return the resolved metadata for the operation
     */
    protected CacheOperationMetadata getCacheOperationMetadata(ContextCall contextCall,
                                                               CacheOperation operation, Method method, Class<?> targetClass) {

        CacheOperationCacheKey cacheKey = new CacheOperationCacheKey(operation, method, targetClass);
        CacheOperationMetadata metadata = this.metadataCache.get(cacheKey);
        if (metadata == null) {
            synchronized (cacheKey){
                metadata = this.metadataCache.get(cacheKey);
                if(metadata != null){
                    return metadata;
                }
                KeyGenerator operationKeyGenerator = null;
                if (StringUtils.hasText(operation.getKeyGenerator())) {
                    operationKeyGenerator = (KeyGenerator) contextCall.getBeanFactory().getBean(operation.getKeyGenerator());
                }
//                else {
//                    operationKeyGenerator = contextCall.getKeyGenerator();
//                }
                AbstractMethodHolder abstractMethodHolder = this.getMethodHolder(contextCall, operation);
                Set<String> cacheNames = operation.getCacheNames();
                Map<String,Cache> caches = new HashMap<>(cacheNames.size());
                for (String cacheName : cacheNames) {
                    Cache cache = contextCall.getCacheManager().getCache(cacheName, operation.getReadSourceName(), operation.getWriteSourceName());
                    caches.put(cacheName, cache);
                }

                metadata = new CacheOperationMetadata(operation,caches,operationKeyGenerator,abstractMethodHolder);
                this.metadataCache.put(cacheKey, metadata);
            }

        }
        return metadata;
    }

    private AbstractMethodHolder getMethodHolder(ContextCall contextCall, CacheOperation cacheOperation) {
        AbstractMethodHolder abstractMethodHolder = null;
        if (cacheOperation instanceof CacheableOperation) {
            CacheableOperation cacheableOperation = (CacheableOperation) cacheOperation;
            if (StringUtils.hasText(cacheableOperation.getMethodHolder())) {
                abstractMethodHolder = (AbstractMethodHolder) contextCall.getBeanFactory().getBean(cacheableOperation.getMethodHolder());
            } else if (cacheableOperation.isSlide()) {
                abstractMethodHolder = contextCall.getDefaultSlideMethodHolder();
            } else {
                abstractMethodHolder = contextCall.getDefaultCacheableMethodHolder();
            }
        } else if (cacheOperation instanceof CachePutOperation) {
            CachePutOperation cachePutOperation = (CachePutOperation) cacheOperation;
            if (StringUtils.hasText(cachePutOperation.getMethodHolder())) {
                abstractMethodHolder = (AbstractMethodHolder) contextCall.getBeanFactory().getBean(cachePutOperation.getMethodHolder());
            } else {
                abstractMethodHolder = contextCall.getDefaultCachePutMethodHolder();
            }
        }
        return abstractMethodHolder;
    }


    /**
     * 通过 注解Class 获取配置的所有注解context配置
     *
     * @param operationClass
     * @return
     */
    public Collection<CacheOperationContext> get(Class<? extends CacheOperation> operationClass) {
        Collection<CacheOperationContext> result = this.contexts.get(operationClass);
        return (result != null ? result : Collections.<CacheOperationContext>emptyList());
    }

    /**
     * 获得所有的 {@link CacheOperationContext}
     *
     * @return
     */
    public Collection<List<CacheOperationContext>> getAllContexts() {
        return contexts.values();
    }


    private boolean determineSyncFlag() {
        List<CacheOperationContext> cacheOperationContexts = this.contexts.get(CacheableOperation.class);
        if (cacheOperationContexts == null) {  // no @Cacheable operation at all
            return false;
        }
        for (CacheOperationContext context : cacheOperationContexts) {
            if (((CacheableOperation) context.getCacheOperationMetadata().getOperation()).isSync()) {
                return true;
            }
        }
        return false;
    }

    private boolean determineSlideFlag() {
        List<CacheOperationContext> cacheOperationContexts = this.contexts.get(CacheableOperation.class);
        if (cacheOperationContexts == null) {  // no @Cacheable operation at all
            return false;
        }
        for (CacheOperationContext context : cacheOperationContexts) {
            if (((CacheableOperation) context.getCacheOperationMetadata().getOperation()).isSlide()) {
                return true;
            }
        }
        return false;
    }

    /**
     * 缓存配置的Context
     * 例如
     * 1:	配置了1个{@link Cacheable} 就生成1个CacheOperationContext
     * 2:  配置了2个{@link Cacheable} 就生成2个CacheOperationContext
     * 3.配置了2个{@link Cacheable} ,配置1个{@link CachePut} 就生成3个CacheOperationContext
     */
    public class CacheOperationContext {

        @Getter
        private CacheOperationMetadata cacheOperationMetadata;

        public CacheOperationContext(CacheOperationMetadata cacheOperationMetadata) {
            this.cacheOperationMetadata = cacheOperationMetadata;
        }

        public MethodMetadata getMethodMetadata() {
            return CacheOperationContexts.this.methodMetadata;
        }

    }

    public static abstract class ContextCall {
        public abstract CacheManager getCacheManager();

        public abstract BeanFactory getBeanFactory();

        public abstract KeyGenerator getKeyGenerator();

        public abstract AbstractMethodHolder getDefaultCacheableMethodHolder();

        public abstract AbstractMethodHolder getDefaultCachePutMethodHolder();

        public abstract DefaultSlideMethodHolder getDefaultSlideMethodHolder();

        public abstract DefaultCacheAnnotationParser getCacheAnnotationParser();
    }


}