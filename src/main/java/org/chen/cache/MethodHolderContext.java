package org.chen.cache;

import org.chen.cache.cacheOperation.CacheOperation;
import org.chen.cache.cacheOperation.CacheOperationInvoker;
import org.chen.cache.manager.Cache;
import lombok.Getter;

/**
 * @author: allen.chen
 * @date: 2017/8/6
 */
public class MethodHolderContext {
    @Getter
    private final Cache cache;

    @Getter
    private final CacheOperation cacheOperation;

    @Getter
    private final CacheOperationInvoker invoker;

    public MethodHolderContext(Cache cache, CacheOperation cacheOperation, CacheOperationInvoker invoker) {
        this.cache = cache;
        this.cacheOperation = cacheOperation;
        this.invoker = invoker;
    }
}
