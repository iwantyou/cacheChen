package org.chen.cache.manager;

/**
 * @author: Allen
 * @date: 2017/7/8
 */
public class RedisConfig {

    /**
     * 默认 读 的redis库名
     */
    private String defaultReadSourceName;
    /**
     * 默认 写 的redis库名
     */
    private String defaultWriteSourceName;
    /**
     * redis 数据源 配置
     */
    private RedisSource [] sources;

    /**
     * 是否支持 缓存key 的分布式锁
     * 一般用于集群多服务的情况
     */
    private boolean distributedLock;

    public String getDefaultReadSourceName() {
        return defaultReadSourceName;
    }

    public void setDefaultReadSourceName(String defaultReadSourceName) {
        this.defaultReadSourceName = defaultReadSourceName;
    }

    public String getDefaultWriteSourceName() {
        return defaultWriteSourceName;
    }

    public void setDefaultWriteSourceName(String defaultWriteSourceName) {
        this.defaultWriteSourceName = defaultWriteSourceName;
    }

    public RedisSource[] getSources() {
        return sources;
    }

    public boolean isDistributedLock() {
        return distributedLock;
    }

    public void setDistributedLock(boolean distributedLock) {
        this.distributedLock = distributedLock;
    }

    public void setSources(RedisSource ... sources) {
        RedisSource [] addSources = new  RedisSource [sources.length];
        for (int i = 0; i < sources.length; i++) {
            addSources[i] = sources[i];
        }
        if(this.sources == null){
            this.sources = addSources;
        } else {
            RedisSource [] arr = new RedisSource[this.sources.length + sources.length];
            System.arraycopy(arr,0,this.sources,0,this.sources.length);
            System.arraycopy(arr,this.sources.length, addSources,0,addSources.length);
            this.sources = arr;
        }

    }

}
